======
README
======

:Info: See <https://bitbucket.org/blackhook/cryptocalc> for repo.
:Author: BARRY Thierno, TAVERNIER Sonny, BOUAZZOUNI Mohamed Amine, KERKAR Mina, NUEL Guillaume (ImmortalPC), DECK Léa (dla)
:Date: $Date: 2013-11-03 $
:Revision: $Revision: 1 $
:Description: Projet pour le cours de Master 2 de Cryptis:  Développement de logiciels cryptographiques. Le projet concerne la création d'une calculette cryptographique.


**Screenshot**
--------------
.. image:: https://bitbucket.org/blackhook/cryptocalc/raw/master/doc/Screenshot.png
	:alt: Screenshot
	:width: 400px
	:align: center
	:target: https://bitbucket.org/blackhook/cryptocalc/raw/master/doc/Screenshot.png

|

**Description**
---------------
Projet pour le cours de Master 2 de Cryptis: Développement de logiciels cryptographiques. Le projet concerne la création d'une calculette cryptographique.

| Ce programme comprend les fonctionnalité suivantes:

- Une interface graphique en Qt4 / Qt5 unifiée.
- Une interface qui permet d'unifier la lisaison Qt <-> algo de crypto.
- Une liste d'outils pour manipuler rotations binaires. (cf src/Tools.h)
- Les algo de crypto sont écrit en C99 et sont indépendament de l'interface graphique.
- Cryptage/Décryptage et Hachage de fichier et/ou de texte.
- Algo de chiffrement symétrique:
	- AES-128, AES-192, AES-256
	- DES
	- 3DES
	- `Chacha (ou Salsa20) <http://fr.wikipedia.org/wiki/Salsa20>`_: `chacha.c <http://bxr.su/OpenBSD/usr.bin/ssh/chacha.c>`_ (**NON** **IMPLÉMENTÉ**)
	- `Poly1305-AES <http://en.wikipedia.org/wiki/Poly1305-AES>`_: `Poly1305.c <http://cr.yp.to/mac.html>`_ (**NON** **IMPLÉMENTÉ**)
	- `Blowfish <http://fr.wikipedia.org/wiki/Blowfish>`_
	- `Twofish <http://fr.wikipedia.org/wiki/Twofish>`_ (**NON** **IMPLÉMENTÉ**)
	- `RC4 <http://fr.wikipedia.org/wiki/RC4>`_ (**NON** **TERMINÉ**)
	- `RC6 <http://fr.wikipedia.org/wiki/RC6>`_ (Aucune attaque connue !!!)
	- `Serpent <http://fr.wikipedia.org/wiki/Serpent_%28cryptographie%29>`_ (Meilleur attaque: 11 tours sur 32 tours !) (**NON** **IMPLÉMENTÉ**)

- Des fonctions de hachages:
	- MD5
	- SHA1
	- SHA-256
	- SHA-512

- Chiffrement et déchiffrement suivant plusieurs modes CBC, ECB.
- Calcul de MAC-CBC ou de H-MAC de fichiers

*Note*: OpenSSL vient juste d'intégrer Chacha et Poly1305

Cette liste n'est pas exhaustive, si des idées chatouillent votre esprit n'hesitez pas ;) :D

|

**Répartition** **du** **travail**
----------------------------------
Amine:
	- MD5, SHA1, SHA-256, SHA-512
Thierno:
	- AES (128 - 192 - 256)
Sonny:
	- DES, hackThePlanet
Mina:
	- RC4 , RC6
Léa:
	- Serpent, Blowfish
Guillaume:
	- GUI, Core, Tests unitaires, ???

|

Architecture du projet
----------------------

::

	.
	├── bin                       <- Dossier des binaires
	│   ├── CryptoCalc            <- Executable principale
	│   └── CryptoCalc_UnitTest   <- Executable pour les tests unitaires
	├── doc                       <- Dossier de documentation
	│   ├── rapport.odt
	│   ├── rapport.pdf
	│   ├── Screenshot.png
	│   ├── Sujet-projet.pdf
	│   └── Système-de-communication.txt
	├── obj/                      <- Dossier temporaire pour les *.o et les moc_*
	├── src                       <- Dossier des fichiers *.cpp et *.h
	│   ├── Core
	│   │   ├── Base64File.cpp    <- Filtre pour écrire en base64 depuis la ligne de comande.
	│   │   ├── Base64File.h
	│   │   ├── CryptoStream.cpp  <- Surcouche pour lire indifféremment un fichier ou une entrée directe. 
	│   │   ├── CryptoStream.h
	│   │   ├── GuiBridge.cpp     <- Liaison entre le GUI et la partie Crypto. Collecte aussi les algo disponnible pour le GUI.
	│   │   └── GuiBridge.h
	│   ├── Crypto                <- Dossier des algo de crypto
	│   │   ├── 3des/
	│   │   ├── aes/
	│   │   ├── blowfish/
	│   │   ├── des/
	│   │   ├── example/
	│   │   ├── md5/
	│   │   ├── rc4/
	│   │   ├── rc6/
	│   │   ├── serpent/
	│   │   ├── SHA-1/
	│   │   ├── xor/
	│   │   └── Tools.h           <- Boite à outils: RotateByte (1100->1001), PrintByte (int,char -> binaire),...
	│   ├── GUI                   <- Interface graphique Qt.
	│   │   ├── MainWindow.cpp
	│   │   └── MainWindow.h
	│   └── main.cpp              <- Le main. C'est ici que l'on peut ajouter / supprimer des algo de crypto.
	├── test                      <- Dossier des tests unitaires
	│   ├── CryptoTest.cpp        <- C'est ici que sont implémenté les tests unitaires GUI et Crypto
	│   ├── CryptoTest.h
	│   ├── GUI
	│   │   └── MainWindowTest.h  <- Liaison pour les setter et getter supplémentaires
	│   └── mainTest.cpp          <- Le main pour les tests unitaires
	├── CryptoCalc.pro            <- Projet pour qmake et QtCreator
	├── README.rst                <- Le README :D
	├── UnitTest.sh               <- Pour compiler et lancer les tests unitaires.
	└── UseOpenSSl.sh             <- Pour voir la différence entre CryptoCalc et OpenSSL


|

**Format** **du** **code** **et** **Normes**
--------------------------------------------
- Le code est en UTF-8
- Le code est indenté avec des tabulations réel (\\t)
- Le code est prévus pour être affiché avec une taille de 4 pour les tab (\\t)
- Les fins de lignes sont de type LF (\\n)
- IDE utilisé: QtCreator
- Les commentaires sont au format doxygen.

|

**Outils nécessaires**
----------------------
- g++
- Qt4 / Qt5
- GMP (libgmp-dev)

|

**Compilation**
---------------
Compilation::

	qmake
	make release -j2

Utilisation classique::

	cd bin/
	./CryptoCalc

Pour lancer les tests unitaires::

	./UnitText.sh

|

**Documentation**
-----------------
| Pour plus d'informations voir la documentation dans le dossier **/doc/**
| Énoncer du projet: `/doc/Sujet-projet.pdf <https://bitbucket.org/blackhook/cryptocalc/raw/master/doc/Sujet-projet.pdf>`_
| Rapport du projet: `/doc/Rapport.pdf <https://bitbucket.org/blackhook/cryptocalc/raw/master/doc/Rapport.pdf>`_

|

**IDE** **RST**
---------------
RST créé grâce au visualiseur: https://github.com/anru/rsted
