TEMPLATE		= app
TARGET			= CryptoCalc
DEPENDPATH		+= src
INCLUDEPATH		+= src
VPATH			+= src
SUBDIRS			+= src

# Utilisation de GMP ?
#LIBS			+= -lgmpxx -lgmp

CONFIG			+= debug_and_release
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
# Paralléllisation.
# OpenMP n'existe que les version récente de MacOS => On le désactive donc
!macx:LIBS		+= -fopenmp

DESTDIR = $$PWD/bin/
MOC_DIR = $$PWD/obj/$$QT_VERSION/
RCC_DIR = $$PWD/obj/$$QT_VERSION/
UI_DIR = $$PWD/obj/$$QT_VERSION/
release:OBJECTS_DIR = $$PWD/obj/$$QT_VERSION/release/
debug:OBJECTS_DIR = $$PWD/obj/$$QT_VERSION/debug/

win32{
	MOC_DIR              = obj/$$QT_VERSION/win/
	RCC_DIR              = obj/$$QT_VERSION/win/
	UI_DIR               = obj/$$QT_VERSION/win/
	release:OBJECTS_DIR  = obj/$$QT_VERSION/win/release/
	debug:OBJECTS_DIR    = obj/$$QT_VERSION/win/debug/
}

QMAKE_CXXFLAGS			+= -W -Wall -Wextra -Wdeprecated -Wuninitialized -Wunused-variable -Wvariadic-macros -Wredundant-decls -Wunused-macros -Wundef -Wwrite-strings -Wredundant-decls -Winit-self -Wpointer-arith -fopenmp
#QMAKE_LFLAGS			+= -W -Wall -Wextra -Wdeprecated -Wuninitialized -Wunused-variable -Wvariadic-macros -Wredundant-decls -Wunused-macros -Wundef -Wfloat-equal -Wwrite-strings -Wredundant-decls -Winit-self -Wpointer-arith
QMAKE_CXXFLAGS_DEBUG	+= -ggdb3
QMAKE_LFLAGS_DEBUG		+= -ggdb3
QMAKE_CXXFLAGS_RELEASE	+= -s
QMAKE_LFLAGS_RELEASE	+= -s

#QMAKE_CC = g++-4.8
#QMAKE_CXX = g++-4.8
#QMAKE_LINK = g++-4.8
#linux-g++:QMAKE_LFLAGS_DEBUG += -fsanitize=address -fno-omit-frame-pointer
#linux-g++:QMAKE_CXXFLAGS_DEBUG += -fsanitize=address -fno-omit-frame-pointer

linux-g++:QMAKE_CXXFLAGS += -fstack-protector -fstack-protector-all -Wstack-protector

# Si on est en mode unit test, on ajuste.
# Pour passer en mode tests unitaires:
# $ qmake CONFIG+=qtestlib
qtestlib {
	CONFIG	+= qtestlib
	DEFINES	+= UNIT_TEST_ENABLE
	TARGET	= $${TARGET}_UnitTest
}


# Input

SOURCES += \
	src/main.cpp \
	test/CryptoTest.cpp \
	src/Core/CryptoStream.cpp \
	src/Core/GuiBridge.cpp \
	src/GUI/MainWindow.cpp \
	src/Crypto/des/des.cpp \
	src/Crypto/xor/xor.cpp \
	src/Crypto/example/example.cpp \
	src/Crypto/3des/3des.cpp \
	src/Core/Base64File.cpp \
	#aes
	src/Crypto/aes/aes.cpp \
	src/Crypto/aes/aes_core.cpp \
	# md5
	src/Crypto/md5/md5.cpp \
	# Tests
	test/mainTest.cpp \
	src/Crypto/blowfish/blowfish.cpp \
	src/Crypto/serpent/serpent.cpp \
	src/Crypto/SHA-1/SHA-1.cpp \
	src/Crypto/rc4/rc4.cpp \
	src/Crypto/rc6/rc6.cpp

HEADERS += \
	src/Core/CryptoStream.h \
	src/Core/GuiBridge.h \
	src/GUI/MainWindow.h \
	src/Crypto/des/des.h \
	src/Crypto/Tools.h \
	src/Crypto/xor/xor.h \
	src/Crypto/example/example.h \
	src/Crypto/3des/3des.h \
	src/Core/Base64File.h \
	#aes
	src/Crypto/aes/aes.h \
	src/Crypto/aes/aes_core.h \
	# md5
	src/Crypto/md5/md5.h \
	# Tests
	test/GUI/MainWindowTest.h \
	test/CryptoTest.h \
	src/Crypto/blowfish/blowfish.h \
	src/Crypto/serpent/serpent.h \
	src/Crypto/blowfish/blowfish_data.h \
	src/Crypto/SHA-1/SHA1.h \
	src/Crypto/rc4/rc4.h \
	src/Crypto/rc6/rc6.h


qtestlib {
	SOURCES -= src/main.cpp
}else{
	SOURCES -= test/mainTest.cpp
	SOURCES -= test/CryptoTest.cpp
	HEADERS -= test/CryptoTest.h
	HEADERS -= test/GUI/MainWindowTest.h
}
