#include <QApplication>
#include <QTextCodec>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>
#include <QDebug>
#include <QFile>
#include "GUI/MainWindow.h"
#include "Core/GuiBridge.h"
// Liste des fonctions de cryptage
#include "Crypto/example/example.h"
#include "Crypto/des/des.h"
#include "Crypto/3des/3des.h"
#include "Crypto/xor/xor.h"
#include "Crypto/aes/aes.h"
#include "Crypto/md5/md5.h"
#include "Crypto/SHA-1/SHA1.h"
#include "Crypto/serpent/serpent.h"
#include "Crypto/blowfish/blowfish.h"
#include "Crypto/rc6/rc6.h"
#include "Crypto/rc4/rc4.h"
#include "Core/CryptoStream.h"
#include "Core/Base64File.h"


/***************************************************************************//*!
* @brief Main
* @param[in] argc		Nombre d'argument dans argv
* @param[in] argv		Liste des arguments passé à l'executable
* @return 0 Si tout s'est bien passé. Supperieur à 0 SINON
*/
int main( int argc, char* argv[] )
{
	QApplication app(argc, argv);

#	if QT_VERSION <= 0x050000
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf8"));
	QTextCodec::setCodecForTr(QTextCodec::codecForName("utf8"));
#	endif

	// Traduction des chaînes prédéfinies par Qt dans notre langue
	QString locale = QLocale::system().name();
	QTranslator translator;
	translator.load(QString("qt_") + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	app.installTranslator(&translator);

	// Ici on enregistre les fonctions de cryptage
	GuiBridge::registerGUI("Example", Crypt::example);
	GuiBridge::registerGUI("XOR-CBC", Crypt::xor_cbc);
	GuiBridge::registerGUI("DES", Crypt::des);
	GuiBridge::registerGUI("3DES", Crypt::des3);
	GuiBridge::registerGUI("AES-128-ECB", Crypt::aes_128_ECB);
	GuiBridge::registerGUI("AES-192-ECB", Crypt::aes_192_ECB);
	GuiBridge::registerGUI("AES-256-ECB", Crypt::aes_256_ECB);
	GuiBridge::registerGUI("AES-128-CBC", Crypt::aes_128_CBC);
	GuiBridge::registerGUI("AES-192-CBC", Crypt::aes_192_CBC);
	GuiBridge::registerGUI("AES-256-CBC", Crypt::aes_256_CBC);
	GuiBridge::registerGUI("Serpent", Crypt::serpent);
	GuiBridge::registerGUI("MD5", Crypt::calcul_md5);
	GuiBridge::registerGUI("SHA1", Crypt::calcul_sha1);
	GuiBridge::registerGUI("Blowfish-CBC", Crypt::blowfish_CBC);
	GuiBridge::registerGUI("Blowfish-ECB", Crypt::blowfish_ECB);
	GuiBridge::registerGUI("RC6-ECB", Crypt::rc6_ecb);
	GuiBridge::registerGUI("RC6-CBC", Crypt::rc6_cbc);
	GuiBridge::registerGUI("RC4-CBC", Crypt::rc4_cbc);
	GuiBridge::registerGUI("RC4-ECB", Crypt::rc4_ecb);


	/***************************************************************************
	* Prise en charge de la ligne de commande:
	* openssl enc -des -base64 -k ABCDEFGH
	*/
	if( argc > 1 ){
		bool isEncrypt = true;
		bool outBase64 = false;
		QString key;
		const GuiBridge::HashInfo* algo = 0;
		QStringList args(QCoreApplication::arguments());
		for( int i=1; i<argc; ++i ){
			if( args.at(i) == "enc" ){
				isEncrypt = true;
			}else if( args.at(i) == "dec" ){
				isEncrypt = false;
			}else if( args.at(i) == "-base64" ){
				outBase64 = true;
			}else if( args.at(i) == "-k" ){
				key = args.at(i+1);
				++i;
			}else if( args.at(i).startsWith('-') ){
				algo = GuiBridge::lookForCipher(args.at(i).mid(1));
			}
		}
		if( algo != 0 ){
			bool ret = false;
			CryptoStream::CryptoStream_t cs;
			cs.m_flags = CryptoStream::CS_READ_FILE | CryptoStream::CS_WRITE_FILE;
			cs.reader.m_fp = new QFile;
			cs.reader.m_fp->open(stdin, QIODevice::ReadOnly);
			if( outBase64 ){
				cs.writer.m_fp = new Base64File(false, true);
			}else{
				cs.writer.m_fp = new QFile;
			}
			cs.writer.m_fp->open(stdout, QIODevice::WriteOnly);

			if( algo->flags & GuiBridge::CF_func0k ){
				// bool AES( CryptoStream* stream )
				ret = algo->func.func0key(&cs);
			}else if( algo->flags & GuiBridge::CF_func0kED ){
				// bool AES( CryptoStream* stream, bool isEncryptMod )
				ret = algo->func.func0keyEncDec(&cs, isEncrypt);
			}else if( algo->flags & GuiBridge::CF_func1k ){
				// bool AES( CryptoStream* stream, const char pass[], size_t passSize, bool isEncryptMod )
				ret = algo->func.func1key(&cs, key.toUtf8().data(), key.size(), isEncrypt);
			}else if( algo->flags & GuiBridge::CF_func2k ){
				// bool AES( CryptoStream* stream, const char pass[], size_t passSize, const char pass2[], size_t pass2Size, bool isEncryptMod )
				ret = algo->func.func2keys(&cs, key.mid(0,key.size()/2).toUtf8().data(), key.size()/2, key.mid(key.size()/2).toUtf8().data(), key.mid(key.size()/2).size(), isEncrypt);
			}else{
				qDebug() << "La fonction a un format improbable !!! " << algo->flags;
			}

			delete cs.reader.m_fp;
			delete cs.writer.m_fp;
			return (int)ret;
		}else{
			qDebug() << "Usage incorrect:\n\t" << argv[0] << "[enc|dec] [-Algo] [-base64] [-k 'My Key']";
			qDebug() << "Algo possible:";
			QStringList tmp = GuiBridge::getCipherList();
			for( int i=0; i<tmp.size(); ++i )
				qDebug() << "\t-" << tmp.at(i).toUtf8().data();
			return 0;
		}
	}

	// Affichage de la fenêtre
	MainWindow mw;
	mw.show();

	return app.exec();
}
