#include "CryptoStream.h"
#include <stdio.h>
#include <QByteArray>
#include <QFile>
#include <stdarg.h>
#include "GuiBridge.h"


/***************************************************************************//*!
* @brief Permet de lire des données à crypter.
* @param[in] cs				Le flux a lire.
* @param[in] size			Taille max a lire et à mettre dans {result}
* @param[in,out] result		Résultat de la lecture.
* @return Nombre de bits lus et écrit dans {result} (Nécéssairement inf. à size)
* @memberof CryptoStream
*/
int CryptoStream::read( CryptoStream::CryptoStream_t* cs, int size, char* result )
{
	if( cs->m_flags & CS_READ_FILE ){
		qint64 ret = cs->reader.m_fp->read(result, size);
		if( cs->m_gui )
			cs->m_gui->setProgress(cs->reader.m_fp->pos()*100/cs->m_readerSize);
		return ret;
	}

	QByteArray tmp(cs->reader.m_str->mid(cs->m_fpos, size));
	memcpy(result, tmp.constData(), tmp.size());
	cs->m_fpos += tmp.size();
	if( cs->m_gui )
		cs->m_gui->setProgress(cs->m_fpos*100/cs->m_readerSize);
	return tmp.size();
}


/***************************************************************************//*!
* @brief Permet de lire d'écrire les données qui ont été cryptées.
* @param[in] cs				Le flux a écrire.
* @param[in] size			Taille a lire dans {result}
* @param[in] result			Données a écrire.
* @return Nombre de bits écrit. (Doit être égale à {size})
* @memberof CryptoStream
*/
void CryptoStream::write( CryptoStream::CryptoStream_t* cs, int size, const char* result )
{
	if( cs->m_flags & CS_WRITE_FILE ){
		cs->writer.m_fp->write(result, size);
		return ;
	}

	cs->writer.m_str->append(result,size);
}


/***************************************************************************//*!
* @brief Permet de logguer une information de débug
* @param[in] cs				Le flux IO.
* @param[in] fmt			Le format (cf printf)
* @param[in] ...			cf Printf
* @return[NONE]
* @memberof CryptoStream
*
* @note Préférer l'utilisation de cryptoLog( cs, fmt, ... ) et cryptoLogF( cs, fmt, ... )
*/
void CryptoStream::log( CryptoStream::CryptoStream_t* cs, const char fmt[], ... )
{
	va_list args;
	va_start(args, fmt);

	enum { BUFFER_SIZE = 5000 };

	char buffer[BUFFER_SIZE+1] = {0};
	vsnprintf(buffer, BUFFER_SIZE, fmt, args);
	va_end(args);

	if( cs->m_log ){
		cs->m_log->write(buffer);
		cs->m_log->flush();
	}

	if( cs->m_gui )
		cs->m_gui->emitLog(buffer);
}


/***************************************************************************//*!
* @brief Permet d'obtenir la quantité de donnée dans le {reader}.
* @param[in] cs				Le flux IO.
* @return >0
*
* @warning Peut ne pas renvoyer la bonne valeur si:
* - Le fichier est un flux (ex: stdin).
*
* @memberof CryptoStream
*/
qint64 CryptoStream::readerSize( const CryptoStream_t* cs )
{
	if( (cs->m_flags & CS_READ_FILE) )
		return cs->reader.m_fp->size();
	return cs->m_readerSize;
}
