#include "Base64File.h"
#include <QByteArray>


/***************************************************************************//*!
* @brief Constructeur
* @param[in] decodeInput		Décoder les read ?
* @param[in] encodeOutput		Encoder en base64 les write ?
* @return[NONE]
*/
Base64File::Base64File( char decodeInput, char encodeOutput )
{
	m_decodeInput = decodeInput;
	m_encodeOutput = encodeOutput;

	m_buffWriter = new QByteArray();
	m_buffWriter->reserve(3);// Il faut 3 char
}


/***************************************************************************//*!
* @brief Destructeur
* @return[NONE]
* @note Va flush le reste de buffer
*/
Base64File::~Base64File()
{
	if( m_encodeOutput && m_buffWriter->size() )
		QFile::writeData(m_buffWriter->toBase64().data(), m_buffWriter->toBase64().size());
	delete m_buffWriter;
}


/***************************************************************************//*!
* @brief cf QFile::readData
* @param[in,out] data	Tableau de sortie.
* @param[in] len		Taille max du tableau et quantité de donner a lire au MAX.
* @return La quantité de donnée lue.
*/
qint64 Base64File::readData( char* data, qint64 len )
{
	if( !m_decodeInput )
		return QFile::readData(data, len);
	throw "[TODO] Code the function Base64File::readData";
	return len;
}


/***************************************************************************//*!
* @brief Constructeur
* @param[in] decodeInput	Les donnée a lire
* @param[in] len			Taille du tableau {decodeInput}.
* @return La quantité de donnée écrite.
*/
qint64 Base64File::writeData( const char* data, qint64 len )
{
	if( !m_encodeOutput )
		return QFile::writeData(data, len);

	int size = 0;
	qint64 ret = 0;

	m_buffWriter->append(data, len);

	// On cherche le bon I%3
	for( size=0; (m_buffWriter->size()-size)%3 != 0; ++size );

	QByteArray tmp(m_buffWriter->left(m_buffWriter->size()-size).toBase64());

	ret = QFile::writeData(tmp.data(), tmp.size());

	if( size == 0 ){
		m_buffWriter->clear();
	}else{
		m_buffWriter->remove(0, m_buffWriter->size()-size);
	}

	return ret;
}
