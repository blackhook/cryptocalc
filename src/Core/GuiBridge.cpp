#include "GuiBridge.h"
#include <QMap>
#include <QComboBox>
#include <QFile>
#include <QCoreApplication>
#include <QSharedPointer>
#include <QByteArray>
#include "CryptoStream.h"


typedef QMap<QString,GuiBridge::HashInfo>	Map;
typedef Map::Iterator						MapIter;
Map											G_listOfCipherFunc;//!< Liste des fonction de cryptage recencées.

// Déclaration du type
Q_DECLARE_METATYPE(QSharedPointer<QByteArray>)



/***************************************************************************//*!
* @brief Permet d'appeler la fonction de crypto avec les données à crypter.
* Fonction de crypto à 1 clé.
* @param[in] parent			Le parent associé au thread.
* @param[in] cryptoName		Nom de la fonction de crypto.
* @param[in] isEncryptMod	Mode: Chiffrement (TRUE) OU déchiffrement (FALSE)
* @param[in] data			Les données a crypter.
* @param[in] isDataFile		Es que {data} est un fichier ?
* @param[in] key			La clé de cryptage a utiliser.
* @param[in] isKeyFile		Es que {key} est un fichier ?
* @param[in] output			Nom du fichier de sortie. (Vide ou NULL si pas d'utilisation de fichier)
* @return[NONE]
*/
GuiBridge::GuiBridge( QObject* parent, const QString& cryptoName, bool isEncryptMod, const QByteArray& data, bool isDataFile, const QByteArray& key, bool isKeyFile, const QByteArray& output )
	: QThread(parent),m_cryptoName(cryptoName),m_isEncryptMod(isEncryptMod),m_data(data),m_isDataFile(isDataFile),m_key(key),m_isKeyFile(isKeyFile),m_isKey2File(false),m_output(output)
{
}


/***************************************************************************//*!
* @brief Permet d'appeler la fonction de crypto avec les données à crypter.
* Fonction de crypto à 2 clés.
* @param[in] parent			Le parent associé au thread.
* @param[in] cryptoName		Nom de la fonction de crypto.
* @param[in] isEncryptMod	Mode: Chiffrement (TRUE) OU déchiffrement (FALSE)
* @param[in] data			Les données a crypter.
* @param[in] isDataFile		Es que {data} est un fichier ?
* @param[in] key			La clé de cryptage a utiliser.
* @param[in] isKeyFile		Es que {key} est un fichier ?
* @param[in] key2			La clé n°2 de cryptage a utiliser.
* @param[in] isKey2File		Es que {key2} est un fichier ?
* @param[in] output			Nom du fichier de sortie. (Vide ou NULL si pas d'utilisation de fichier)
* @return[NONE]
*/
GuiBridge::GuiBridge( QObject* parent, const QString& cryptoName, bool isEncryptMod, const QByteArray& data, bool isDataFile, const QByteArray& key, bool isKeyFile, const QByteArray& key2, bool isKey2File, const QByteArray& output )
	: QThread(parent),m_cryptoName(cryptoName),m_isEncryptMod(isEncryptMod),m_data(data),m_isDataFile(isDataFile),m_key(key),m_isKeyFile(isKeyFile),m_key2(key2),m_isKey2File(isKey2File),m_output(output)
{
}


/***************************************************************************//*!
* @brief Point d'entré du thread.
* @return[NONE]
*/
void GuiBridge::run()
{
	callCipherFunc();
}


/***************************************************************************//*!
* @brief Permet d'enregistrer une fonction de cryptage. La fonction sera dispo dans
* le GUI. La fonction de cryptage nécéssite 2 clés.
* @param[in] text			Le nom d'affichage de la fonction de cryptage.
* @param[in] func			Un pointeur vers la fonction de cryptage.
* @return[NONE]
*/
void GuiBridge::registerGUI( const char text[], CipherFunction_0Key func )
{
	if( G_listOfCipherFunc.contains(text) )
		throw QString("Un hash porte déjà le nom <")+text+QString(">");

	// On supprime le vieu log
	QFile::remove(text+QString(".log"));
	G_listOfCipherFunc[text].flags = CF_func0k;
	G_listOfCipherFunc[text].func.func0key = func;
}


/***************************************************************************//*!
* @brief Permet d'enregistrer une fonction de cryptage. La fonction sera dispo dans
* le GUI. La fonction de cryptage nécéssite 2 clés.
* @param[in] text			Le nom d'affichage de la fonction de cryptage.
* @param[in] func			Un pointeur vers la fonction de cryptage.
* @return[NONE]
*/
void GuiBridge::registerGUI( const char text[], CipherFunction_0KeyEncDec func )
{
	if( G_listOfCipherFunc.contains(text) )
		throw QString("Un hash porte déjà le nom <")+text+QString(">");

	// On supprime le vieu log
	QFile::remove(text+QString(".log"));
	G_listOfCipherFunc[text].flags = CF_func0kED;
	G_listOfCipherFunc[text].func.func0keyEncDec = func;
}


/***************************************************************************//*!
* @brief Permet d'enregistrer une fonction de cryptage. La fonction sera dispo dans
* le GUI. La fonction de cryptage nécéssite 2 clés.
* @param[in] text			Le nom d'affichage de la fonction de cryptage.
* @param[in] func			Un pointeur vers la fonction de cryptage.
* @return[NONE]
*/
void GuiBridge::registerGUI( const char text[], CipherFunction_1Key func )
{
	if( G_listOfCipherFunc.contains(text) )
		throw QString("Un hash porte déjà le nom <")+text+QString(">");

	// On supprime le vieu log
	QFile::remove(text+QString(".log"));
	G_listOfCipherFunc[text].flags = CF_func1k | CF_canUnCipher | CF_1KeyNeeded;
	G_listOfCipherFunc[text].func.func1key = func;
}


/***************************************************************************//*!
* @brief Permet d'enregistrer une fonction de cryptage. La fonction sera dispo dans
* le GUI. La fonction de cryptage nécéssite 2 clés.
* @param[in] text			Le nom d'affichage de la fonction de cryptage.
* @param[in] func			Un pointeur vers la fonction de cryptage.
* @return[NONE]
*/
void GuiBridge::registerGUI( const char text[], CipherFunction_2Keys func )
{
	if( G_listOfCipherFunc.contains(text) )
		throw QString("Un hash porte déjà le nom <")+text+QString(">");

	// On supprime le vieu log
	QFile::remove(text+QString(".log"));
	G_listOfCipherFunc[text].flags = CF_func2k | CF_canUnCipher | CF_1KeyNeeded | CF_2KeyNeeded;
	G_listOfCipherFunc[text].func.func2keys = func;
}


/***************************************************************************//*!
* @brief Permet de générer l'interface graphique pour les fonctions de crypto.
* @param[in] parent		Le parent dans lequel on va ajouter les fonctions de crypto.
* @return[NONE]
*/
void GuiBridge::makeGUI( QComboBox* parent )
{
	for( MapIter i=G_listOfCipherFunc.begin(), iEnd=G_listOfCipherFunc.end(); i!=iEnd; ++i )
	{
		parent->addItem(i.key());
	}
}


/***************************************************************************//*!
* @brief Permet d'obtenir des info sur une fonction de cryptage déjà enregistrée.
* @param[in] cryptoName		Nom graphique de la fonction de crypto.
* @return[NONE]
*/
const GuiBridge::HashInfo* GuiBridge::cipherInfo( const QString& cryptoName )
{
	if( G_listOfCipherFunc.contains(cryptoName) )
		return &G_listOfCipherFunc[cryptoName];
	return 0;
}


/***************************************************************************//*!
* @brief Permet d'appeler la fonction de crypto avec les données à crypter
* @return[NONE]
*/
void GuiBridge::callCipherFunc()
{
	if( !G_listOfCipherFunc.contains(m_cryptoName) ){
		emit error(tr("La fonction <%1> n'existe pas dans la base de donnée").arg(m_cryptoName));
		return ;
	}

	CryptoStream::CryptoStream_t cs;
	cs.m_flags = 0;
	cs.m_gui = this;

	// Lecture du fichier à crypter
	if( m_isDataFile ){
		cs.reader.m_fp = new QFile(m_data);
		if( !cs.reader.m_fp->open(QIODevice::ReadOnly) ){
			emit error(tr("Le fichier <%1> n'est pas trouvable.").arg(m_data.data()));
			return ;
		}
		cs.m_readerSize = cs.reader.m_fp->size();
		cs.m_flags |= CryptoStream::CS_READ_FILE;
		if( cs.m_readerSize <= 0 ){
			delete cs.reader.m_fp;
			emit error(tr("Le fichier <%1> est vide.\n=> Rien n'a crypter/décrypter").arg(m_data.data()));
			return ;
		}
	}else{
		cs.reader.m_str = new QByteArray(m_data);
		cs.m_fpos = 0;
		cs.m_readerSize = m_data.size();
		if( cs.m_readerSize <= 0 ){
			emit error(tr("Rien n'a crypter/décrypter"));
			return ;
		}
	}

	bool isOutputFile=!m_output.isNull() && !m_output.isEmpty();

	// Création du fichier de sortie
	if( isOutputFile ){
		cs.writer.m_fp = new QFile(m_output);
		if( !cs.writer.m_fp->open(QIODevice::Truncate | QIODevice::WriteOnly) ){
			if( m_isDataFile ){
				delete cs.reader.m_fp;
			}else{
				delete cs.reader.m_str;
			}
			emit error(tr("Le fichier <%1> ne peut être crée. (ACCESS DENY)").arg(m_output.data()));
			return ;
		}
		cs.m_flags |= CryptoStream::CS_WRITE_FILE;
	}else{
		cs.writer.m_str = new QByteArray;
	}


	// Clé de cryptage n°1
	QByteArray pass1;
	if( (G_listOfCipherFunc[m_cryptoName].flags & CF_1KeyNeeded) ){
		if( m_isKeyFile ){
			QFile file(m_key);
			if( !file.open(QIODevice::ReadOnly) ){
				// Nétoyage
				if( m_isDataFile )
					delete cs.reader.m_fp;
				if( isOutputFile )
					delete cs.writer.m_fp;
				emit error(tr("Le fichier <%1> n'est pas trouvable.\n=> Clé invalide.").arg(m_key.data()));
				return ;
			}
			pass1 = file.readAll();
		}else{
			pass1 = m_key;
		}
		if( pass1.isEmpty() || pass1.isNull() ){
			// Nétoyage
			if( m_isDataFile ){
				delete cs.reader.m_fp;
			}else{
				delete cs.reader.m_str;
			}
			if( isOutputFile ){
				delete cs.writer.m_fp;
			}else{
				delete cs.writer.m_str;
			}
			emit error(tr("L'algo <%1> nécéssite une clé non vide").arg(m_cryptoName));
			return ;
		}
	}


	// Clé de cryptage n°2
	QByteArray pass2;
	if( (G_listOfCipherFunc[m_cryptoName].flags & CF_2KeyNeeded) ){
		if( m_isKey2File ){
			QFile file(m_key2);
			if( !file.open(QIODevice::ReadOnly) ){
				// Nétoyage
				if( m_isDataFile )
					delete cs.reader.m_fp;
				if( isOutputFile )
					delete cs.writer.m_fp;
				emit error(tr("Le fichier <%1> n'est pas trouvable.\n=> Clé n°2 invalide.").arg(m_key2.data()));
				return ;
			}
			pass2 = file.readAll();
		}else{
			pass2 = m_key2;
		}
		if( pass2.isEmpty() || pass2.isNull() ){
			// Nétoyage
			if( m_isDataFile ){
				delete cs.reader.m_fp;
			}else{
				delete cs.reader.m_str;
			}
			if( isOutputFile ){
				delete cs.writer.m_fp;
			}else{
				delete cs.writer.m_str;
			}
			emit error(tr("L'algo <%1> nécéssite une clé n°2 non vide").arg(m_cryptoName));
			return ;
		}
	}

	// Ouverture du log
	cs.m_log = new QFile(m_cryptoName+".log");
	cs.m_log->open(QIODevice::WriteOnly | QIODevice::Append);
	qint64 lastSize = cs.m_log->pos();

	try{
		if( G_listOfCipherFunc[m_cryptoName].flags & CF_func0k ){
			// bool AES( CryptoStream* stream )
			if( !G_listOfCipherFunc[m_cryptoName].func.func0key(&cs) ){
				emit error(tr("Une erreur inconnue est survenue lors du cryptage.\nAvez vous bien remplis tous les champs ?"));
			}
		}else if( G_listOfCipherFunc[m_cryptoName].flags & CF_func0kED ){
			// bool AES( CryptoStream* stream, bool isEncryptMod )
			if( !G_listOfCipherFunc[m_cryptoName].func.func0keyEncDec(&cs, this->m_isEncryptMod) ){
				emit error(tr("Une erreur inconnue est survenue lors du cryptage.\nAvez vous bien remplis tous les champs ?"));
			}
		}else if( G_listOfCipherFunc[m_cryptoName].flags & CF_func1k  ){
			// bool AES( CryptoStream* stream, const char pass[], size_t passSize, bool isEncryptMod )
			if( !G_listOfCipherFunc[m_cryptoName].func.func1key(&cs, pass1.constData(), pass1.size(), this->m_isEncryptMod) ){
				emit error(tr("Une erreur inconnue est survenue lors du cryptage.\nAvez vous bien remplis tous les champs ?"));
			}
		}else if( G_listOfCipherFunc[m_cryptoName].flags & CF_func2k  ){
			// bool AES( CryptoStream* stream, const char pass[], size_t passSize, const char pass2[], size_t pass2Size, bool isEncryptMod )
			if( !G_listOfCipherFunc[m_cryptoName].func.func2keys(&cs, pass1.constData(), pass1.size(), pass2.constData(), pass2.size(), this->m_isEncryptMod) ){
				emit error(tr("Une erreur inconnue est survenue lors du cryptage.\nAvez vous bien remplis tous les champs ?"));
			}
		}else{
			throw "La fonction a un format improbable !!!";
		}
	}catch( const char err[] ){
		emit error(err);
	}

	// On ferme le fichier de log
	if( lastSize != cs.m_log->pos() )
		cs.m_log->write("--------------------------------------------------------------------------------\n");
	// On supprime le fichier s'il est vide.
	if( !cs.m_log->size() ){
		cs.m_log->close();
		cs.m_log->remove();
	}
	delete cs.m_log;

	if( isOutputFile ){
		emit cipherFuncFinish( QSharedPointer<QByteArray>(new QByteArray()) );
	}else{
		emit cipherFuncFinish( QSharedPointer<QByteArray>(cs.writer.m_str) );
	}

	// On fait le nétoyage
	if( m_isDataFile ){
		delete cs.reader.m_fp;
	}else{
		delete cs.reader.m_str;
	}
	if( isOutputFile ){
		delete cs.writer.m_fp;
	}else{
		// Auto delete par QSharedPointer
		//delete cs.writer.m_str;
	}
}


/***************************************************************************//*!
* @brief [SLOT] Permet de mesurer l'avancement d'un cryptage/décryptage
* @param[in] percent		Pourcentage d'avancement
* @return[NONE]
*
* @note Va simplement émettre un signal.
*/
void GuiBridge::setProgress( unsigned char percent )
{
	emit progress(percent);
}


/***************************************************************************//*!
* @brief [SLOT] Permet d'envoyer un message de log.
* @param[in] msg		Le message a logguer
* @return[NONE]
*
* @note Va simplement émettre un signal.
*/
void GuiBridge::emitLog( const char msg[] )
{
	emit log(QString(msg));
}


/***************************************************************************//*!
* @brief Permet de chercher l'ago de crypto {cryptoName} dans la list des algo connu.
* @param[in] cryptoName		Le nom de l'ago a chercher.
* @return NULL Si {cryptoName] n'est pas trouvé. Un pointeur GuiBridge::HashInfo valide SINON.
*
* @note Recherche insenssible à la case.
*/
const GuiBridge::HashInfo* GuiBridge::lookForCipher( const QString& cryptoName )
{
	QString cryptoNameLower = cryptoName.toLower();
	for( MapIter i=G_listOfCipherFunc.begin(), iEnd=G_listOfCipherFunc.end(); i!=iEnd; ++i )
	{
		if( i.key() == cryptoName || i.key().toLower() == cryptoNameLower )
			return &(i.value());
	}
	return 0;
}


/***************************************************************************//*!
* @brief Permet d'obtenir la liste des algo de crypto disponnible.
* @return La liste des algo de crypto disponnible.
*/
QStringList GuiBridge::getCipherList()
{
	return G_listOfCipherFunc.keys();
}
