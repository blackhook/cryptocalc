#ifndef CRYPTOSTREAM_H
#define CRYPTOSTREAM_H

//#include <stdio.h>// Pour FILE
#include <QtGlobal>
class QByteArray;
class GuiBridge;
class QFile;

/***************************************************************************//*!
* @brief Ce module permet de gérer des flux de lecture / d'écriture.<br />
* Permet de faire abstraction du type de lecture: file OR GUI input.<br />
* Et permet aussi de faire abstraction du type d'écriture: file OR GUI output.<br />
* Ce système basic permet de faire la liaison entre le GUI et les algo de Crypto.<br />
* Dans cette classe, aucun appel GUI ne peut être effectué.<br />
* Cette classe est suposée être utilisé dans un thread.<br />
* <br />
* Utilisation:
* @code
* // ******************************************************************************
* // * @brief AES
* // * @param[in,out] stream	Data streamer
* // * @param[in] pass			Password to use. (May be binary -> NON ASCII, May not contain \0)
* // * @param[in] passSize		Size of password.
* // * @return don't know.
* // **
* bool AES( CryptoStream* stream, const char pass[], size_t passSize )
* {
* 	char msg[512];// buffer
* 	int r;// nb byte read
*
* 	while( / * Working cond * / )
* 	{
* 		// We read 512b from {stream} and we write it into {msg}
* 		r = read(stream, 512, msg);
*
* 		// Warning {r} may contain less than 512b.
*
* 		// Encrypt my {msg} data
*
* 		// We write 512b from {stream} and we write it into {msg}
* 		write(ret, 512, msg);
* 	}
*
* 	// Note. For showing some GUI message:
* 	//QMessageBox::critical()
* 	//QMessageBox::information()
* 	//QMessageBox::question()
* 	//QMessageBox::warning()
*
*	throw new "Will prompt this message in a QMessageBox::critical";
*
* 	return true;
* }
* @endcode
*/
namespace CryptoStream
{
	enum {
		CS_READ_FILE	= 1,//!< Le reader est un fichier.
		CS_WRITE_FILE	= 2//!< Le writer est un fichier.
	};

	/*!
	 * @brief Flux vers une zone à lire / écrire.
	 * @note Les membres de CryptoStream sont tous PUBLIC
	 * @memberof CryptoStream
	 */
	typedef struct CryptoStream_t {
		char				m_flags;//!< Permet déterminer ce qui doit être utilisé.
		union {
			QFile*			m_fp;//!< Flux de lecture (fichier).
			QByteArray*		m_str;//!< Flux de lecture (texte).
		} reader;
		qint64				m_readerSize;//!< Taille total du reader. ( > 0 )
		size_t				m_fpos;//!< Position dans {reader.m_str}
		union {
			QFile*			m_fp;//!< Flux d'écriture.
			QByteArray*		m_str;//!< Flux d'écriture (texte).
		} writer;
		GuiBridge*			m_gui;//!< Class a informer sur la progression. (Peut être NULL)
		QFile*				m_log;//!< Fichier de log. (Peut être NULL)
		// Auto init
		CryptoStream_t(){ m_flags=0; reader.m_fp=0; m_readerSize=0; m_fpos=0; writer.m_fp=0; m_gui=0; m_log=0; }
	} CryptoStream_t;

	int read( CryptoStream_t* cs, int size, char* result );
	void write( CryptoStream_t* cs, int size, const char* result );
	void log( CryptoStream::CryptoStream_t* cs, const char fmt[], ... );
	qint64 readerSize( const CryptoStream_t* cs );


	/***************************************************************************//*!
	* @brief Permet de logguer une information de débug avec nom du fichier d'appel et ligne.
	* @param[in] cs				Le flux IO.
	* @param[in] fmt			Le format (cf printf)
	* @param[in] ...			cf Printf
	* @return[NONE]
	* @memberof CryptoStream
	*
	* @note Pour une version sans [FILE:line] voir cryptoLog
	*/
#	define cryptoLogF( cs, fmt, ... ) CryptoStream::log( cs, "[" __FILE__ ":%d]:\n\t" fmt "\n", __LINE__, ##__VA_ARGS__ )

	/***************************************************************************//*!
	* @brief Permet de logguer une information de débug SANS non du fichier d'appel et ligne.
	* @param[in] cs				Le flux IO.
	* @param[in] fmt			Le format (cf printf)
	* @param[in] ...			cf Printf
	* @return[NONE]
	* @memberof CryptoStream
	*
	* @note Pour une version avec [FILE:line] voir cryptoLogF
	*/
#	define cryptoLog( cs, fmt, ... ) CryptoStream::log( cs, fmt, ##__VA_ARGS__ )
}
#endif // CRYPTOSTREAM_H
