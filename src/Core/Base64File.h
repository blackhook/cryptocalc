#ifndef BASE64FILE_H
#define BASE64FILE_H

#include <QFile>
class QByteArray;


/***************************************************************************//*!
* @brief Cette classe permet d'encode en Base64 avant d'écrire dans un fichier
* @todo Faire la fonction Base64File::readData pour le cas du décodage.
*/
class Base64File : public QFile
{
	private:
		bool			m_decodeInput;//!< Décoder les read ?
		bool			m_encodeOutput;//!< Encoder en base64 les write ?
		QByteArray*		m_buffWriter;//!< Buffer d'écriture

	public:
		Base64File( char decodeInput, char encodeOutput );
		~Base64File();

	protected:
		virtual qint64 readData( char* data, qint64 maxlen );
		virtual qint64 writeData( const char* data, qint64 len );
};

#endif// BASE64FILE_H
