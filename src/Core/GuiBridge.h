#ifndef GUIBRIDGE_H
#define GUIBRIDGE_H

#ifdef Q_OS_LINUX
#	include <stdint-gcc.h>
#else
#	include <stdint.h>
#endif
#include <QString>
#include <QThread>
#include <QSharedPointer>


// On évite de faire un lien avec CryptoStream.h
namespace CryptoStream {
	struct CryptoStream_t;
}
// On évite de faire un lien avec Qt
class QComboBox;
class QByteArray;


/***************************************************************************//*!
* @brief Cette classe permet de faire la lisaison entre le système graphique et
* le système CryptoStream.<br />
* <br />
* Utilisation:
* @code
* int main(){
*	GuiBridge::registerGUI("MD5", ...);
*	GuiBridge::registerGUI("AES-256-CBC"...);
*	GuiBridge::registerGUI("AES-256-ECB"...);
*	GuiBridge::registerGUI(...);
* }
*
* ...
*
* // Lors de la génération de l'interface graphique:
* QComboBox* lst = new QComboBox(this);
* GuiBridge::makeGUI(lst);
*
* ...
*
* // Puis pour l'appel d'un algo de cryptage/décryptage:
* GuiBridge* runner = new GuiBridge(this, "MD5", "texte a crypter", ....);
* // Slot local pour la récéption des données finales
* connect(runner, SIGNAL(cipherFuncFinish(QSharedPointer<QByteArray>)), this, SLOT(cihperFuncFinish(QSharedPointer<QByteArray>)));
* // Slot pour la barre de progression
* connect(runner, SIGNAL(progress(unsigned char)), this, SLOT(progress(unsigned char)));
* // Slot pour les erreurs
* connect(runner, SIGNAL(error(QString)), this, SLOT(onError(QString)));
* // On lance le thread
* runner->start();
* @endcode
*/
class GuiBridge : public QThread
{
	Q_OBJECT

	public:
		/*!
		 * @brief Fonction de cryptage dans clé et sans différenciation encrypt/decrypt
		 * Format de la fonction de cryptage:
		 * bool AAAA( CryptoStream::CryptoStream* ioData );
		 * @param[in,out] ioData	Les données à lire et à écrire.
		 * @return TRUE si tous s'est bien passé. FALSE sinon.
		 */
		typedef bool (*CipherFunction_0Key)( CryptoStream::CryptoStream_t* ioData );
		/*!
		 * @brief Fonction de cryptage sans clé et avec différenciation encrypt/decrypt
		 * Format de la fonction de cryptage:
		 * bool AAAA( CryptoStream::CryptoStream* ioData, bool isEncryptMod );
		 * @param[in,out] ioData	Les données à lire et à écrire.
		 * @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter
		 * @return TRUE si tous s'est bien passé. FALSE sinon.
		 */
		typedef bool (*CipherFunction_0KeyEncDec)( CryptoStream::CryptoStream_t* ioData, bool isEncryptMod );
		/*!
		 * @brief Fonction de cryptage avec 1 clé et avec différenciation encrypt/decrypt
		 * Format de la fonction de cryptage:
		 * bool AAAA( CryptoStream::CryptoStream* ioData, const char key[], uint32_t keySize, bool isEncryptMod );
		 * @param[in,out] ioData	Les données à lire et à écrire.
		 * @param[in] key			La clé a utiliser. (Peut contenir '\0' !)
		 * @param[in] keySize		La taille de {key}
		 * @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter
		 * @return TRUE si tous s'est bien passé. FALSE sinon.
		 */
		typedef bool (*CipherFunction_1Key)( CryptoStream::CryptoStream_t* ioData, const char key[], uint32_t keySize, bool isEncryptMod );
		/*!
		 * @brief Fonction de cryptage avec 2 clés et avec différenciation encrypt/decrypt
		 * Format de la fonction de cryptage:
		 * bool AAAA( CryptoStream::CryptoStream* ioData, const char key[], uint32_t keySize, const char key2[], uint32_t key2Size, bool isEncryptMod );
		 * @param[in,out] ioData	Les données à lire et à écrire.
		 * @param[in] key			La clé a utiliser. (Peut contenir '\0' !)
		 * @param[in] keySize		La taille de {key}
		 * @param[in] key2			La clé n°2 a utiliser. (Peut contenir '\0' !)
		 * @param[in] key2Size		La taille de {key2}
		 * @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter
		 * @return TRUE si tous s'est bien passé. FALSE sinon.
		 */
		typedef bool (*CipherFunction_2Keys)( CryptoStream::CryptoStream_t* ioData, const char key[], uint32_t keySize, const char key2[], uint32_t key2Size, bool isEncryptMod );

		/*!
		 * @brief Flags pour GuiBridge::HashInfo::flags
		 */
		enum {
			CF_1KeyNeeded		= 1,//!< {func} a besoin de 1 clé
			CF_2KeyNeeded		= 2,//!< {func} a besoin de 2 clés
			CF_canUnCipher		= 4,//!< {func} est inversible ? {func} peux déchiffrer ?
			CF_func0k			= 8, //!< {func} au format {CipherFunction_0Key}
			CF_func0kED			= 16,//!< {func} au format {CipherFunction_0KeyEncDec}
			CF_func1k			= 32,//!< {func} au format {CipherFunction_1Key}
			CF_func2k			= 64 //!< {func} au format {CipherFunction_2Keys}
		};

		/*!
		 * @brief Info pour les fonctions de crypto: flags d'utilisation + la fonction de crypto à appeler.
		 */
		typedef struct {
			char						flags;//!< Flags pour la fonction de crypto (cf CF_*)
			union func {
				CipherFunction_0Key			func0key;//!< La fonction de crypto.
				CipherFunction_0KeyEncDec	func0keyEncDec;//!< La fonction de crypto.
				CipherFunction_1Key			func1key;//!< La fonction de crypto.
				CipherFunction_2Keys		func2keys;//!< La fonction de crypto.
			} func;//!< Liste des fonctions de crypto possible
		} HashInfo;


	private:
		QString			m_cryptoName;//!< Nom de la fonction de crypto.
		bool			m_isEncryptMod;//!< Mode: chiffrement (TRUE) ou déchiffrement (FALSE).
		QByteArray		m_data;//!< Les données a crypter.
		bool			m_isDataFile;//!< Es que {data} est un fichier ?
		QByteArray		m_key;//!< La clé de cryptage a utiliser.
		bool			m_isKeyFile;//!< Es que {key} est un fichier ?
		QByteArray		m_key2;//!< La clé n°2 de cryptage a utiliser.
		bool			m_isKey2File;//!< Es que {key2} est un fichier ?
		QByteArray		m_output;//!< Nom du fichier de sortie. (Vide ou NULL si pas d'utilisation de fichier)


	private:
		void callCipherFunc();


	public:
		GuiBridge( QObject* parent, const QString& cryptoName, bool isEncryptMod, const QByteArray& data, bool isDataFile, const QByteArray& key, bool isKeyFile, const QByteArray& output );
		GuiBridge( QObject* parent, const QString& cryptoName, bool isEncryptMod, const QByteArray& data, bool isDataFile, const QByteArray& key, bool isKeyFile, const QByteArray& key2, bool isKey2File, const QByteArray& output );
		void run();
		void setProgress( unsigned char percent );
		void emitLog( const char msg[] );
		static void registerGUI( const char text[], CipherFunction_0Key func );
		static void registerGUI( const char text[], CipherFunction_0KeyEncDec func );
		static void registerGUI( const char text[], CipherFunction_1Key func );
		static void registerGUI( const char text[], CipherFunction_2Keys func );
		static void makeGUI( QComboBox* parent );
		static const HashInfo* cipherInfo( const QString& cryptoName );
		static const HashInfo* lookForCipher( const QString& cryptoName );
		static QStringList getCipherList();


	signals:
		void cipherFuncFinish( QSharedPointer<QByteArray> data );
		void progress( unsigned char percent );
		void error( QString msg );
		void log( QString msg );
};
#endif // GUIBRIDGE_H
