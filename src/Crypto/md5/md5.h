#ifndef _MD5_H
#define _MD5_H

#include "../../Core/CryptoStream.h"

#ifndef uint8
// Byte 1 octet Byte dans la RFC.
#define uint8  unsigned char
#endif

#ifndef uint32
// Word <=> 4 Octets.
#define uint32 unsigned long int
#endif
// Taille du hash MD5 produit
#define MD5_HASHBYTES 16

//Structure definissant le context MD5 avec
// stockage d'etats intermediaires, et un buffer de 64 bytes (octets)
typedef struct
{
	uint32 total[2]; // nombre de bits modulo 2^64 (lsb en premier )
	uint32 state[4]; // A,B,C et D quatre words
	uint8 buffer[64]; // buffer d'entree
}
md5_context;

//void calcul_md5( unsigned char *donnees, unsigned int taille, unsigned char *hash); // La fonction qui
void md5_starts( md5_context *ctx );
void md5_update( md5_context *ctx, uint8 *input, uint32 length );
void md5_finish( md5_context *ctx, uint8 digest[16] );

namespace Crypt {
	bool calcul_md5( CryptoStream::CryptoStream_t* stream );
}

#endif /* md5.h */

