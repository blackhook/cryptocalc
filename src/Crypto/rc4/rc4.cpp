#include "rc4.h"
#include <string>
#include <stdlib.h>
#include "../Tools.h"

enum { BLOCK_SIZE = 256 };//!< Taille de {content} et de {result}
enum{ S_BOX_SIZE = 256 };//!< Taille de la S Box


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {pass} qui est de taille
* {passSize}.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @param[in] pass			La clé a utiliser.
* @param[in] passSize		La taille de la clé {pass}
* @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::rc4_ecb( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	(void) isEncryptMod;
	char S_BOX[S_BOX_SIZE] = {0};
	char buff[BLOCK_SIZE] = {0};
	char result[BLOCK_SIZE] = {0};

	int readSize=0;

	rc4::initKey(pass, passSize, S_BOX);

	while( (readSize=CryptoStream::read(stream, BLOCK_SIZE, buff)) > 0 )
	{
		// Padding
		if( readSize != BLOCK_SIZE ){
			for( unsigned char i=BLOCK_SIZE-readSize; i<readSize; ++i )
				buff[i] = 0;
		}

		rc4::rc4_oneBlock( buff, result, pass, passSize, S_BOX );
		CryptoStream::write(stream, BLOCK_SIZE, result);
	}
	return true;
}


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {pass} qui est de taille
* {passSize} avec un chainage CBC.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @param[in] pass			La clé a utiliser.
* @param[in] passSize		La taille de la clé {pass}
* @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::rc4_cbc( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	unsigned char S_BOX[S_BOX_SIZE]={0};
	char buff[BLOCK_SIZE] = {0};
	char result[BLOCK_SIZE] = {0};
	char prevBlock[BLOCK_SIZE] = {0};

	int readSize=0;

	rc4::initKey(pass, passSize, S_BOX);

#	define cpyBuff( to, from ) for( int ij=0; ij<BLOCK_SIZE; ++ij ){ to[ij]=from[ij]; }
#	define xorBuff( to, from ) for( int ij=0; ij<BLOCK_SIZE; ++ij ){ to[ij]^=from[ij]; }

	while( (readSize=CryptoStream::read(stream, BLOCK_SIZE, buff)) > 0 )
	{
		// Padding
		if( readSize != BLOCK_SIZE ){
			for( unsigned char i=BLOCK_SIZE-readSize; i<readSize; ++i )
				buff[i] = 0;
		}
		if( !isEncryptMod ){
			cpyBuff(result, buff);
			xorBuff(buff, prevBlock);
			cpyBuff(prevBlock, result);
		}
		rc4::rc4_oneBlock( buff, result, S_BOX );
		if( isEncryptMod ){
			cpyBuff(buff, result);
			xorBuff(result, prevBlock);
			cpyBuff(prevBlock, buff);
		}
		CryptoStream::write(stream, BLOCK_SIZE, result);
	}
	return true;
}


void Crypt::rc4::rc4_oneBlock( const char content[], char result[], const char key[], unsigned int keySize )
{
	unsigned char S[S_BOX_SIZE]={0};
	unsigned char temp=0;
	unsigned int i=0, j=0;
	unsigned long k=0;

	// initialisation de la clé
	for( i=0; i<S_BOX_SIZE; i++ )
		S[i] = i;

	j=0;
	for( i=0; i<S_BOX_SIZE; i++ )
	{
		j = (j + S[i] + key[i % keySize]) % S_BOX_SIZE;

		temp = S[i];
		S[i] = S[j];
		S[j] = temp;
	}

	// chiffrement
	i = j = 0;
	for( k=0; k<BLOCK_SIZE; k++ )
	{
		i = (i + 1) % S_BOX_SIZE;
		j = (j + S[i]) % S_BOX_SIZE;

		temp = S[i];
		S[i] = S[j];
		S[j] = temp;

		result[k] = ((S[(S[i] + S[j]) % S_BOX_SIZE]) ^ content[k]);
	}
}


void Crypt::rc4::initKey( const char key[], unsigned int keySize, unsigned char S_BOX[] )
{
	unsigned char temp=0;
	unsigned int i=0, j=0;

	// initialisation de la clé
	for( i=0; i<S_BOX_SIZE; i++ )
		S_BOX[i] = i;

	j=0;
	for( i=0; i<S_BOX_SIZE; i++ )
	{
		j = (j + S_BOX[i] + key[i % keySize]) % S_BOX_SIZE;

		temp = S_BOX[i];
		S_BOX[i] = S_BOX[j];
		S_BOX[j] = temp;
	}
	return S_BOX;
}


void Crypt::rc4::rc4_oneBlock( const char content[], char result[], const unsigned char S_BOX[] )
{
	unsigned char temp=0;
	unsigned int i=0, j=0;
	unsigned long k=0;

	// chiffrement
	i = j = 0;
	for( k=0; k<BLOCK_SIZE; k++ )
	{
		i = (i + 1) % S_BOX_SIZE;
		j = (j + S_BOX[i]) % S_BOX_SIZE;

		temp = S_BOX[i];
		S_BOX[i] = S_BOX[j];
		S_BOX[j] = temp;

		result[k] = ((S_BOX[(S_BOX[i] + S_BOX[j]) % S_BOX_SIZE]) ^ content[k]);
	}
}
