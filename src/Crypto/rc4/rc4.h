#ifndef RC4_h
#define RC4_h
#pragma once


#include "../../Core/CryptoStream.h"

// On met XOR dans un namespace pour éviter les collisions avec d'autres fonctions
namespace Crypt {
	bool rc4_ecb( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
	bool rc4_cbc( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );

	namespace rc4 {
		void initKey( const char key[], unsigned int keySize, unsigned char* S_BOX );
		void rc4_oneBlock( const char content[], char result[], const char key[], unsigned int keySize );
		void rc4_oneBlock( const char content[], char result[], const unsigned char* S_BOX );
	}
}
#endif// RC4_h
