#ifndef SERPENT_H
#define SERPENT_H

#include "../../Core/CryptoStream.h"

// On met Serpent dans un namespace pour éviter les collisions avec d'autres fonctions
namespace Crypt {

bool serpent( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );

	// On met les fonctions interne à 3DES dans un namespace pour éviter les collisions avec d'autres fonctions
	namespace Internal {
	bool serpent_encrypt( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, const char pass2[], size_t pass2Size );
	bool serpent_decrypt( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, const char pass2[], size_t pass2Size );
}
}
#endif // SERPENT_H
