#include "3des.h"
#include "../Tools.h"
#include "../des/des.h"
#if 0// Pour désactiver le log
#	undef cryptoLog
#	undef cryptoLogF
#	define cryptoLog(...)
#	define cryptoLogF(...)
#endif


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {pass} qui est de taille
* {passSize}.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @param[in] pass			La clé a utiliser.
* @param[in] passSize		La taille de la clé {pass}
* @param[in] pass2			La clé n°2 a utiliser.
* @param[in] pass2Size		La taille de la clé {pass2}
* @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::des3( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, const char pass2[], size_t pass2Size, bool isEncryptMod )
{
	Q_UNUSED(passSize);
	Q_UNUSED(pass2Size);
	TYPE cles[16] = {0};
	TYPE cles2[16] = {0};

	generer_cles(pass, cles);
	generer_cles(pass2, cles2);

	if (isEncryptMod)
		return Internal::des3_cbc_encrypt(stream, cles, cles2);

	return Internal::des3_cbc_decrypt(stream, cles, cles2);
}

bool Crypt::Internal::des3_cbc_encrypt(CryptoStream::CryptoStream_t* stream, TYPE* cles, TYPE* cles2)
{
	chiffrer(stream, cles, NULL, "out.txt");
	chiffrer(stream, cles2, "out.txt", "out2.txt");
	chiffrer(stream, cles, "out2.txt", NULL);

	return true;
}


bool Crypt::Internal::des3_cbc_decrypt(CryptoStream::CryptoStream_t* stream, TYPE* cles, TYPE* cles2)
{
	dechiffrer(stream, cles, NULL, "out.txt");
	dechiffrer(stream, cles2, "out.txt", "out2.txt");
	dechiffrer(stream, cles, "out2.txt", NULL);

	return true;
}
