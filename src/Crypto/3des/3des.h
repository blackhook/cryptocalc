#ifndef DES3_H
#define DES3_H

#include "../../Core/CryptoStream.h"

typedef unsigned long long  TYPE;

namespace Crypt {
    bool des3(CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, const char pass2[], size_t pass2Size, bool isEncryptMod);

    namespace Internal {
        bool des3_cbc_encrypt(CryptoStream::CryptoStream_t* stream, TYPE* cles, TYPE* cles2);
        bool des3_cbc_decrypt(CryptoStream::CryptoStream_t* stream, TYPE* cles, TYPE* cles2);
	}
}
#endif // DES3_H
