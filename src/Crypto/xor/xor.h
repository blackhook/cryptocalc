#ifndef XOR_H
#define XOR_H

#include "../../Core/CryptoStream.h"

// On met XOR dans un namespace pour éviter les collisions avec d'autres fonctions
namespace Crypt {

	bool xor_cbc( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );

	// On met les fonctions interne à XOR dans un namespace pour éviter les collisions avec d'autres fonctions
	namespace Internal {
		bool xor_cbc_encrypt( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize );
		bool xor_cbc_decrypt( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize );
	}
}
#endif // XOR_H
