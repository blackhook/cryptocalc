#include "xor.h"
#include "../Tools.h"
#if 0// Pour désactiver le log
#	undef cryptoLog
#	undef cryptoLogF
#	define cryptoLog(...)
#	define cryptoLogF(...)
#endif


/***************************************************************************//*!
* @brief Permet de crypter/décrypter des données {stream} avec la clé {pass} qui est de taille
* {passSize} avec l'algo XOR-CBC.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @param[in] pass			La clé a utiliser.
* @param[in] pasSize		La taille de la clé {pass}
* @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::xor_cbc( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	if( isEncryptMod )
		return Internal::xor_cbc_encrypt(stream,pass,passSize);
	return Internal::xor_cbc_decrypt(stream,pass,passSize);
}


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {pass} qui est de taille
* {passSize} avec l'algo XOR-CBC.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @param[in] pass			La clé a utiliser.
* @param[in] pasSize		La taille de la clé {pass}
* @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::Internal::xor_cbc_encrypt( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize )
{
	cryptoLogF(stream, "Encrypt init");
	char buff;
	char previous_char = 42;
	size_t iPass = 0;
	cryptoLogF(stream, "Encrypt begin");
	while( CryptoStream::read(stream, 1, &buff) == 1 )
	{
		previous_char = (buff ^ previous_char) ^ pass[iPass%passSize];
		CryptoStream::write(stream, 1, &previous_char);
		++iPass;
	}
	cryptoLogF(stream, "Encrypt done");

	//throw "J' ai une erreur divine...";
	return true;
}


/***************************************************************************//*!
* @brief Permet de décrypter des données {stream} avec la clé {pass} qui est de taille
* {passSize} avec l'algo XOR-CBC.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @param[in] pass			La clé a utiliser.
* @param[in] pasSize		La taille de la clé {pass}
* @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::Internal::xor_cbc_decrypt( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize )
{
	char buff;
	char tmp;
	char previous_char = 42;
	size_t iPass = 0;
	while( CryptoStream::read(stream, 1, &buff) == 1 )
	{
		tmp = (buff ^ previous_char) ^ pass[iPass%passSize];
		previous_char = buff;
		CryptoStream::write(stream, 1, &tmp);
		++iPass;
	}

	//throw "J' ai une erreur divine...";
	return true;
}
