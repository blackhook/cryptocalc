#include "SHA1.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define success 1
#define fail 0
#define erreur 2


#define CircularShift(bits,word) \
				(((word) << (bits)) | ((word) >> (32-(bits))))


void AjoutPaddingSHA1(SHA1Context *ctx);
void SHA1TraitementBloc(SHA1Context *ctx);
void calcul_sha1( unsigned char *donnees, unsigned int taille, unsigned char *hash);


/*
  La fonction d'initialisation permet de mettre le contexte a jour pour le calcul du hash.
  les hash intermediaires sont au debut initialises aux valeurs preconises
  1 : 0x67452301;
  2 : 0xEFCDAB89;
  3 : 0x98BADCFE;
  4 : 0x10325476;
  5 : 0xC3D2E1F0;

  les deux entiers indiquant la taille en bits sont initialises a 0
 */



int SHA_INIT( SHA1Context * ctx){

	if(!ctx){

	return 0;
	}
  ctx->Length_Low = 0;
  ctx->Length_High = 0;
  ctx->Message_Block_Index = 0;

  ctx->Intermediate_Hash[0] = 0x67452301;
  ctx->Intermediate_Hash[1] = 0xEFCDAB89;
  ctx->Intermediate_Hash[2] = 0x98BADCFE;
  ctx->Intermediate_Hash[3] = 0x10325476;
  ctx->Intermediate_Hash[4] = 0xC3D2E1F0;

  ctx->Computed = 0;
  ctx->Corrupted = 0;

  return  1;
}


/* La fonction int SHA_Resultat(SHA1Context * , uint8_t hash[HashSize]);
 * va nous retourner la valeur du hash dans la variable hash;
 * Si un des deux parametres est nul on quitte avec une erreur
 * Elle permet aussi de nettoyer la structure contexte
 */

int SHA_Resultat(SHA1Context *ctx , uint8_t hash[HashSize]){

  int i ;

  if(!ctx || !hash){

	  return 0;
  }

  if(ctx->Corrupted){

	return ctx->Corrupted;
  }

  if(!ctx->Computed ){

	  AjoutPaddingSHA1(ctx);
		 for(i=0; i<64; ++i)
		{
		/* effacement du message */
		ctx->Message_Block[i] = 0;

		}
		ctx->Length_Low = 0;    /*effacement des longueurs */
		ctx->Length_High = 0;
		ctx->Computed = 1; /* dire que le hash est calcule */


		}
		for(i = 0; i < HashSize; ++i){

	  hash[i] = ctx->Intermediate_Hash[i>>2]
							>> 8 * ( 3 - ( i & 0x03 ) );

	}

	 return 2;
}

 /* La fonction SHA_Calcul prend en parametres le contexte
  * a mettre a jour ainsi qu'une sequence correspondant a un
  * bout du msg a hasher et la taille de cette derniere et
  * elle produit en sortie un context mis a jour
  */


 int SHA_Calcul(SHA1Context *ctx, const uint8_t *chaine, unsigned  longueur){

   // On est arrive a la fin, donc taille du bloc = 0
   if(!longueur){

	 return success;

   }

   //si nous disposons pas des deux premiers parametres = NULL alors echec
   if(!ctx || !chaine){

	 return fail;
   }

  if(ctx->Computed){

	  ctx->Corrupted = erreur;
	  return erreur;
  }

  if(ctx->Corrupted){

	  return ctx->Corrupted;

  }

  /* Tant qu'on est pas arrive a la fin de la chaine et que la chaine a hasher n'est pas trop longue on continue a diviser la chaine en blocks */
  while(longueur-- && ctx->Corrupted == 0)
  {
	/*constitution des blocs octet par octet*/

	ctx->Message_Block[ctx->Message_Block_Index++] = (*chaine & 0xFF);
	ctx-> Length_Low += 8; // on rajoute à la taille 8 bits = 1 octet
	if(ctx->Length_Low == 0){

	 ctx->Length_High++;
	 if(ctx->Length_High == 0){

	   ctx->Corrupted = 1; // le message est trop long

	}
	}

	if(ctx->Message_Block_Index == 64){

	  SHA1TraitementBloc(ctx);

	}
	//on avance dans la chaine afin de traiter les 64 prochains octets
	chaine++;



}
return success;
 }

void SHA1TraitementBloc(SHA1Context *ctx){

  uint32_t  K;
  int i;
  uint32_t fct;
  uint32_t tmp;
  uint32_t w[80];
  uint32_t a,b,c,d,e;

  // La boucle ci-dessous permet de decouper chaque bloc de 512 bits en 16 mots de 32 bits en big endian

  for(i = 0; i < 16; i++)
	{
		w[i] = ctx->Message_Block[i * 4] << 24;
		w[i] |= ctx->Message_Block[i * 4 + 1] << 16;
		w[i] |= ctx->Message_Block[i * 4 + 2] << 8;
		w[i] |= ctx->Message_Block[i * 4 + 3];
	}

  for(i = 16; i < 80; i++){
	  w[i] = CircularShift(1, w[i-3] ^ w[i-8] ^ w[i-14]  ^ w[i-16]);
  }

  a = ctx->Intermediate_Hash[0];
  b = ctx->Intermediate_Hash[1];
  c = ctx->Intermediate_Hash[2];
  d = ctx->Intermediate_Hash[3];
  e = ctx->Intermediate_Hash[4];

  //Boucle principale
  for(i = 0; i < 80; i++){

   if(i >= 0 && i <= 19){

	 fct = (b & c) | ((~ b) & d);
	 K = 0x5A827999;

   }

   else if(i >=20 && i <= 39){

	  fct = b ^ c ^ d;
	  K = 0x6ED9EBA1;

   }

   else if(i >= 40 && i <= 59){

	  fct = (b & c) | (b & d) | (c & d);
	  K = 0x8F1BBCDC;

   }

   else if(i >= 60 && i <= 79){

	  fct =  b ^ c ^ d;
	  K = 0xCA62C1D6;

   }


	tmp = CircularShift(5,a) + fct + e + K + w[i];
	e = d;
	d = c;
	c = CircularShift(30,b);
	b = a;
	a = tmp;

  }

	ctx->Intermediate_Hash[0] += a;
	ctx->Intermediate_Hash[1] += b;
	ctx->Intermediate_Hash[2] += c;
	ctx->Intermediate_Hash[3] += d;
	ctx->Intermediate_Hash[4] += e;

  ctx->Message_Block_Index = 0;


}



/* Comme on divise le MSG en blocs de 512 l'ideal c'est d'avoir un nombre de bits congrue a 0
 * Modulo 512. Nous commençons d'abord par rajouter un 1 puis suffisamment de 0 pour que le
 * Nombre de bits soit congrue a 448 mod 512 puis nous ajoutons 64 bits correspondant a la
 * taille du msg et donc on obtient une taille congrue a 0 mod 512.
 */



void AjoutPaddingSHA1(SHA1Context *ctx){


	if (ctx->Message_Block_Index > 55)
	{
		ctx->Message_Block[ctx->Message_Block_Index++] = 0x80;
		while(ctx->Message_Block_Index < 64)
		{
			ctx->Message_Block[ctx->Message_Block_Index++] = 0;
		}

		 SHA1TraitementBloc(ctx);

		while(ctx->Message_Block_Index < 56)
		{
			ctx->Message_Block[ctx->Message_Block_Index++] = 0;
		}

   }
	else
	{
		ctx->Message_Block[ctx->Message_Block_Index++] = 0x80;
		while(ctx->Message_Block_Index < 56)
		{

			ctx->Message_Block[ctx->Message_Block_Index++] = 0;
		}
	}

	/*
	 *  les 8 derniers octets correspondent à la taille du msg
	 */
	ctx->Message_Block[56] = ctx->Length_High >> 24;
	ctx->Message_Block[57] = ctx->Length_High >> 16;
	ctx->Message_Block[58] = ctx->Length_High >> 8;
	ctx->Message_Block[59] = ctx->Length_High;
	ctx->Message_Block[60] = ctx->Length_Low >> 24;
	ctx->Message_Block[61] = ctx->Length_Low >> 16;
	ctx->Message_Block[62] = ctx->Length_Low >> 8;
	ctx->Message_Block[63] = ctx->Length_Low;

			SHA1TraitementBloc(ctx);





}


bool Crypt::calcul_sha1(CryptoStream::CryptoStream_t *stream)
{
	int nb_blocs, i;
	int bloc_size = 1000;
	unsigned char buf[bloc_size];
	unsigned char hash[20];
	char strHexHash[40];
	char tmp[3];

	SHA1Context mon_contexte;
	SHA_INIT( &mon_contexte);

	nb_blocs = stream->m_readerSize/bloc_size + (stream->m_readerSize%bloc_size ? 1 : 0);
	for(i=0; i<nb_blocs-1; i++)
	{
		CryptoStream::read(stream, sizeof(buf), (char*)buf);
		SHA_Calcul(&mon_contexte, buf, sizeof(buf));
	}
	memset(buf, 0, sizeof(buf));
	CryptoStream::read(stream, sizeof(buf), (char*)buf);

	SHA_Calcul(&mon_contexte, buf, strlen((char*)buf));
	SHA_Resultat( &mon_contexte, hash );

	memset(strHexHash,0, sizeof(strHexHash));
	for(i=0; i<20; i++)
	{
		sprintf(tmp, "%02x", hash[i]);
		strHexHash[2*i] = tmp[0];
		strHexHash[2*i+1] = tmp[1];
		memset(tmp,0, sizeof(tmp));
	}

	CryptoStream::write(stream, 40, strHexHash);
	return true;
}


