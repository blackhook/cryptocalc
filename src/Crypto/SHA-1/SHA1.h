

#ifndef _SHA1_H_
#define _SHA1_H_

#include "../../Core/CryptoStream.h"

#include<stdint.h>
#define HashSize 20

/*
 *  Cette structure a pour but de stocker le contexte de l'execution
 *de SHA1
 */
typedef struct SHA1Context
{
	uint32_t Intermediate_Hash[HashSize/4]; /* Hash du msg  */

	uint32_t Length_Low;            /* taille en bits      */
	uint32_t Length_High;           /* taille en bits      */

							   /* L'index dans le tableau   */
	int_least16_t Message_Block_Index;
	uint8_t Message_Block[64];      /* blocs de 512 bits      */

	int Computed;               /* indique si le hash est calcule ou pas         */
	int Corrupted;             /* Indique si le hash est corrompu ou pas */
} SHA1Context;

/*
 *  Function Prototypes
 */

int SHA_INIT(  SHA1Context *);
int SHA_Calcul(  SHA1Context *,
				const uint8_t *,
				unsigned int);
int SHA_Resultat( SHA1Context *,
				uint8_t Message_Digest[HashSize]);

namespace Crypt {
	bool calcul_sha1( CryptoStream::CryptoStream_t* stream );
}


#endif
