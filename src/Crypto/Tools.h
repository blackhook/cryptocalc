#include <stdio.h>
#ifdef Q_OS_LINUX
#	include <stdint-gcc.h>
#else
#	include <stdint.h>
#endif
#include "../Core/CryptoStream.h"


/***************************************************************************//*!
* @brief Contient une liste d'outils pour la manipulation des rotations binaires.
*/
namespace Tools {


/***************************************************************************//*!
* @brief Permet de faire une rotation vers la gauche: 1234 => 2341.
* @param[in] real		Le chiffre a décaler.
* @param[in,opt] n		Décalage de {n} bytes (default: 1).
* @return {real} avec un décalage cyclique vers la gauche de {n} bytes.
*/
template<typename T>
T rotateLeft( T real, uint16_t n=1 )
{
	return ((real << n) | (real >> (sizeof(T)*8-n)));
}


/***************************************************************************//*!
* @brief Permet de faire une rotation vers la gauche: 1234 => 2341.
* Version define.
* @param[in] real		Le chiffre a décaler.
* @param[in,opt] n		Décalage de {n} bytes (default: 1).
* @return {real} avec un décalage cyclique vers la gauche de {n} bytes.
*/
#define rotateLeft_def( real, N ) ((real << (N)) | (real >> (sizeof(real)*8-(N))))


/***************************************************************************//*!
* @brief Permet de faire une rotation vers la droite: 1234 => 4123.
* @param[in] real		Le chiffre a décaler.
* @param[in,opt] n		Décalage de {n} bytes (default: 1).
* @return {real} avec un décalage cyclique vers la droite de {n} bytes.
*/
template<typename T>
T rotateRight( T real, uint16_t n=1 )
{
	return ((real >> n) | (real << (sizeof(T)*8-n)));
}


/***************************************************************************//*!
* @brief Permet de faire une rotation vers la droite: 1234 => 4123.
* Version avec define.
* @param[in] real		Le chiffre a décaler.
* @param[in,opt] n		Décalage de {n} bytes (default: 1).
* @return {real} avec un décalage cyclique vers la droite de {n} bytes.
*/
#define rotateRight_def( real, N ) ((real >> (N)) | (real << (sizeof(real)*8-(N))))


/***************************************************************************//*!
* @brief Permet d'afficher les bytes d'un type: int, uint, double, ...
* Version avec define.
* @param[in] real		Le chiffre a afficher.
* @return[NONE]
*/
template<typename T>
void printByte( T real )
{
	for( short i=sizeof(T)*8; i>=0; i-- )
		printf("%c", (real & (1 << i)?'1':'0'));
	printf("\n");
}


/***************************************************************************//*!
* @brief Permet d'afficher les bytes d'un type: int, uint, double, ...
* Version avec define.
* @param[in] cs			Le flux d'IO.
* @param[in] real		Le chiffre a afficher.
* @return[NONE]
*/
template<typename T>
void printByte( CryptoStream::CryptoStream_t* cs, T real )
{
	for( short i=sizeof(T)*8; i>=0; i-- )
		cryptoLog(cs, "%c", (real & (1 << i)?'1':'0'));
	printf("\n");
}

};
