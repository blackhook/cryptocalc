#ifndef RC6_h
#define RC6_h

#include "../../Core/CryptoStream.h"

// On met XOR dans un namespace pour éviter les collisions avec d'autres fonctions
namespace Crypt {
	bool rc6_ecb( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
	bool rc6_cbc( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );

	namespace rc6 {
		bool encrypt_ecb( CryptoStream::CryptoStream_t* stream );
		bool decrypt_ecb( CryptoStream::CryptoStream_t* stream );
		bool encrypt_cbc( CryptoStream::CryptoStream_t* stream );
		bool decrypt_cbc( CryptoStream::CryptoStream_t* stream );
		void rc6_exp_k( const unsigned char keyInit[], int size );
		void rc6_chiffre( const unsigned int data[4], unsigned int result[4] );
		void rc6_dechiffre( const unsigned int data[4], unsigned int result[4] );
	}
}
#endif// RC6_h
