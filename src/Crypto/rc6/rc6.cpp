#include <stdio.h>
#include <string.h>// Pour memcpy
#include "rc6.h"
//#include "../Tools.h"
#if 0// Pour désactiver le log
#	undef cryptoLog
#	undef cryptoLogF
#	define cryptoLog(...)
#	define cryptoLogF(...)
#endif


#define w 32		// la taille du mot en bit
#define r 20		//!< Nombre de rotation

#define P32 0xB7E15163
#define Q32 0x9E3779B9


#define bytes   (w / 8)						// bytes par mot
#define c       ((size + bytes - 1) / bytes)
#define R24     (2 * r + 4)					//!< Taille de la clé
#define lgw     5                       	// log2 du mot --

// Rotations
#define ROTL(x,y) (((x)<<((y)&(w-1))) | ((x)>>(w-((y)&(w-1)))))
#define ROTR(x,y) (((x)>>((y)&(w-1))) | ((x)<<(w-((y)&(w-1)))))

unsigned int G_Key[R24 - 1];//!< La clé globale


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {pass} qui est de taille
* {passSize}.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @param[in] pass			La clé a utiliser.
* @param[in] passSize		La taille de la clé {pass}
* @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::rc6_ecb( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	Crypt::rc6::rc6_exp_k((const unsigned char*)pass, passSize);

	if( isEncryptMod )
		return Crypt::rc6::encrypt_ecb( stream );
	return Crypt::rc6::decrypt_ecb( stream );
}


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {G_Key} qui est de taille
* fixe.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::rc6::encrypt_ecb( CryptoStream::CryptoStream_t* stream )
{
	enum { RECV_BUFF = sizeof(unsigned int)*4 };
	union {
		char ch[RECV_BUFF];
		unsigned int ui[4];
	} buff = {0};
	union {
		char ch[RECV_BUFF];
		unsigned int ui[4];
	} result = {0};
	int readSize = 0;

	while( (readSize=CryptoStream::read(stream, RECV_BUFF, buff.ch)) > 0 )
	{
		// Padding
		if( readSize != RECV_BUFF ){
			for( unsigned char i=RECV_BUFF-readSize; i<readSize; ++i )
				buff.ch[i] = 0;
		}

		rc6_chiffre(buff.ui, result.ui);
		CryptoStream::write(stream, RECV_BUFF, result.ch);
	}
	return true;
}


/***************************************************************************//*!
* @brief Permet de décrypter des données {stream} en mode ecb avec la clé {G_Key}
* qui est de taille fixe.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::rc6::decrypt_ecb( CryptoStream::CryptoStream_t* stream )
{
	enum { RECV_BUFF = sizeof(unsigned int)*4 };
	union {
			char ch[RECV_BUFF];
			unsigned int ui[4];
	} buff = {0};
	union {
			char ch[RECV_BUFF];
			unsigned int ui[4];
	} result = {0};
	int readSize = 0;

	while( (readSize=CryptoStream::read(stream, RECV_BUFF, buff.ch)) > 0 )
	{
		// Padding
		if( readSize != RECV_BUFF ){
			for( char i=RECV_BUFF-readSize; i<readSize; ++i )
				buff.ch[i] = 0;
		}

		rc6_dechiffre(buff.ui, result.ui);
		CryptoStream::write(stream, RECV_BUFF, result.ch);
	}
	return true;
}


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {pass} qui est de taille
* {passSize} avec un chainage CBC.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @param[in] pass			La clé a utiliser.
* @param[in] passSize		La taille de la clé {pass}
* @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::rc6_cbc( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	Crypt::rc6::rc6_exp_k((const unsigned char*)pass, passSize);

	if( isEncryptMod )
		return Crypt::rc6::encrypt_cbc( stream );
	return Crypt::rc6::decrypt_cbc( stream );
}


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {G_Key} qui est de taille
* fixe avec un chainage CBC.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::rc6::encrypt_cbc( CryptoStream::CryptoStream_t* stream )
{
	enum { RECV_BUFF = sizeof(unsigned int)*4 };
	union {
		char ch[RECV_BUFF];
		unsigned int ui[4];
	} buff = {0};
	unsigned int previousBlock[4]={0};// Vecteur d'initialisation à mettre ici.
	union {
		char ch[RECV_BUFF];
		unsigned int ui[4];
	} result = {0};
	int readSize = 0;

#	define cpyBuff( to, from ) {to[0]=from[0]; to[1]=from[1]; to[2]=from[2]; to[3]=from[3];}
#	define xorBuff( res, from ) {res[0]^=from[0]; res[1]^=from[1]; res[2]^=from[2]; res[3]^=from[3];}

	while( (readSize=CryptoStream::read(stream, RECV_BUFF, buff.ch)) > 0 )
	{
		// Padding
		if( readSize != RECV_BUFF ){
			for( unsigned char i=RECV_BUFF-readSize; i<readSize; ++i )
				buff.ch[i] = 0;
		}

		rc6_chiffre(buff.ui, result.ui);
		cpyBuff(buff.ui, result.ui);
		xorBuff(result.ui, previousBlock);
		cpyBuff(previousBlock, buff.ui);
		CryptoStream::write(stream, RECV_BUFF, result.ch);
	}
	return true;
#	undef cpyBuff
#	undef xorBuff
}


/***************************************************************************//*!
* @brief Permet de décrypter des données {stream} en mode CBC avec la clé {G_Key}
* qui est de taille fixe.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::rc6::decrypt_cbc( CryptoStream::CryptoStream_t* stream )
{
	enum { RECV_BUFF = sizeof(unsigned int)*4 };
	union {
			char ch[RECV_BUFF];
			unsigned int ui[4];
	} buff = {0};
	unsigned int previousBlock[4]={0};// Vecteur d'initialisation à mettre ici.
	union {
			char ch[RECV_BUFF];
			unsigned int ui[4];
	} result = {0};
	int readSize = 0;

#	define cpyBuff( to, from ) {to[0]=from[0]; to[1]=from[1]; to[2]=from[2]; to[3]=from[3];}
#	define xorBuff( res, from ) {res[0]^=from[0]; res[1]^=from[1]; res[2]^=from[2]; res[3]^=from[3];}

	while( (readSize=CryptoStream::read(stream, RECV_BUFF, buff.ch)) > 0 )
	{
		// Padding
		if( readSize != RECV_BUFF ){
			for( char i=RECV_BUFF-readSize; i<readSize; ++i )
				buff.ch[i] = 0;
		}
		cpyBuff(result.ui, buff.ui);
		xorBuff(buff.ui, previousBlock);
		cpyBuff(previousBlock, result.ui);
		rc6_dechiffre(buff.ui, result.ui);
		CryptoStream::write(stream, RECV_BUFF, result.ch);
	}
	return true;
#	undef cpyBuff
#	undef xorBuff
}


/***************************************************************************//*!
* @brief Permet de générer la clé.
* @param[in] keyInit	La clé initial a transformer.
* @param[in] size		La taille de la clé.
* @return[NONE]
*
* @note Va modifier la variable globale G_Key.
*/
void Crypt::rc6::rc6_exp_k( const unsigned char keyInit[], int size )
{
	int i=0, j=0, s=0, v=0;
	unsigned int L[(32 + bytes - 1) / bytes]={0};
	unsigned int A=0, B=0;

	L[c - 1] = 0;
	for (i = size - 1; i >= 0; i--)
		L[i / bytes] = (L[i / bytes] << 8) + keyInit[i];

	G_Key[0] = P32;
	for (i = 1; i <= 2 * r + 3; i++)
		G_Key[i] = G_Key[i - 1] + Q32;

	A = B = i = j = 0;
	v = R24;
	if (c > v) v = c;
	v *= 3;

	for (s = 1; s <= v; s++)
	{
		A = G_Key[i] = ROTL(G_Key[i] + A + B, 3);
		B = L[j] = ROTL(L[j] + A + B, A + B);
		i = (i + 1) % R24;
		j = (j + 1) % c;
	}
}


/***************************************************************************//*!
* @brief Permet de crypter un block {data} dans {result}
* @param[in] data		Les données a crypter
* @param[in] result		Les données cryptées
* @return[NONE]
*
* @note Va utiliser la variable globale G_Key.
*/
void Crypt::rc6::rc6_chiffre( const unsigned int data[4], unsigned int result[4] )
{
	unsigned int A=0, B=0, C=0, D=0, t=0, u=0, x=0;
	int i=0;

	A = data[0];
	B = data[1];
	C = data[2];
	D = data[3];
	B += G_Key[0];
	D += G_Key[1];
	for (i = 2; i <= 2 * r; i += 2)
	{
		t = ROTL(B * (2 * B + 1), lgw);
		u = ROTL(D * (2 * D + 1), lgw);
		A = ROTL(A ^ t, u) + G_Key[i];
		C = ROTL(C ^ u, t) + G_Key[i + 1];
		x = A;
		A = B;
		B = C;
		C = D;
		D = x;
	}
	A += G_Key[2 * r + 2];
	C += G_Key[2 * r + 3];
	result[0] = A;
	result[1] = B;
	result[2] = C;
	result[3] = D;
}


/***************************************************************************//*!
* @brief Permet de décrypter un block {data} dans {result}
* @param[in] data		Les données a décrypter
* @param[in] result		Les données décryptées
* @return[NONE]
*
* @note Va utiliser la variable globale G_Key.
*/
void Crypt::rc6::rc6_dechiffre( const unsigned int data[4], unsigned int result[4] )
{
	unsigned int A=0, B=0, C=0, D=0, t=0, u=0, x=0;
	int i=0;

	A = data[0];
	B = data[1];
	C = data[2];
	D = data[3];
	C -= G_Key[2 * r + 3];
	A -= G_Key[2 * r + 2];
	for (i = 2 * r; i >= 2; i -= 2)
	{
		x = D;
		D = C;
		C = B;
		B = A;
		A = x;
		u = ROTL(D * (2 * D + 1), lgw);
		t = ROTL(B * (2 * B + 1), lgw);
		C = ROTR(C - G_Key[i + 1], t) ^ u;
		A = ROTR(A - G_Key[i], u) ^ t;
	}
	D -= G_Key[1];
	B -= G_Key[0];
	result [0] = A;
	result [1] = B;
	result [2] = C;
	result [3] = D;
}
