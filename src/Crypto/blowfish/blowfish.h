#ifndef BLOWFISH_H
#define BLOWFISH_H

#include "../../Core/CryptoStream.h"

#define NBR_T 16 // nombre de tour

/**
 * @struct	BLW_S
 * @brief	objet contenant les boxes nécéssaires adaptées à la clé utilisée
 *
 */
typedef struct {
	unsigned long P[16+2];	/*!< P-array */
	unsigned long S[4][256];/*!< S-boxes */
}BLW_S, *BLW;


namespace Crypt {
bool blowfish_ECB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
bool blowfish_CBC(CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, const char p_pass[], size_t p_passSize, bool isEncryptMod );
bool blowfish_CFB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
bool blowfish_OFB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
bool blowfish_NOFB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );

	namespace Internal {
		unsigned long BLW_F(BLW PSboxes, unsigned long x);
		void BLW_initPSboxes(BLW PSboxes, const char pass[], size_t passSize);
		void BLW_initPSboxes_FirstBLW(BLW PSboxes);
		void BLW_SubEncrypt( BLW p_PSboxes, unsigned long* p_xL, unsigned long* p_xR );

		bool BLW_encrypt_ECB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize );
		bool BLW_decrypt_ECB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize );
		bool BLW_encrypt_CBC( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, const char p_pass2[], size_t p_passSize2 );
		bool BLW_decrypt_CBC( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize );
	}
}

#endif // BLOWFISH_H
