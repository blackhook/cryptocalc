#include "blowfish.h"
#include "blowfish_data.h"
#include "../Tools.h"
#if 0// Pour désactiver le log
#	undef cryptoLog
#	undef cryptoLogF
#	define cryptoLog(...)
#	define cryptoLogF(...)
#endif

/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {pass} qui est de taille
* {passSize}, selon l'algorithme BlowFish.
* @param[in,out] p_stream		Les données a crypter. C'est aussi ici que l'on écrit
*								le résultat.
* @param[in] p_pass				La clé a utiliser.
* @param[in] p_passSize			La taille de la clé {pass}
* @param[in] p_isEncryptMod		TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::blowfish_ECB( CryptoStream::CryptoStream_t* p_stream, const char p_pass[], size_t p_passSize, bool p_isEncryptMod )
{
	if( p_isEncryptMod )
		// On nous demande de crypter
		return Internal::BLW_encrypt_ECB(p_stream, p_pass, p_passSize);
	// On nous demande de décrypter
	return Internal::BLW_decrypt_ECB(p_stream, p_pass, p_passSize);
}


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {pass} qui est de taille
* {passSize}, selon l'algorithme BlowFish avec le chainage CBC.
* @param[in,out] p_stream		Les données a crypter. C'est aussi ici que l'on écrit
*								le résultat.
* @param[in] p_pass				La clé a utiliser.
* @param[in] p_passSize			La taille de la clé {pass}
* @param[in] p_isEncryptMod		TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::blowfish_CBC( CryptoStream::CryptoStream_t* p_stream, const char p_pass[], size_t p_passSize, const char p_pass2[], size_t p_passSize2, bool p_isEncryptMod )
{
	if( p_isEncryptMod )
		// On nous demande de crypter
		return Internal::BLW_encrypt_CBC(p_stream, p_pass, p_passSize, p_pass2, p_passSize2);
	// On nous demande de décrypter
	return Internal::BLW_decrypt_CBC(p_stream, p_pass, p_passSize);
}


/***************************************************************************//**
 * @brief Fonction F du shéma de Feistel
 * @param[in] p_PSboxes		P-array et S-boxes
 * @param[in] p_x			Xl (32 bits)
 * @return unsigned long	32 bits de résultat
 */
unsigned long Crypt::Internal::BLW_F( BLW p_PSboxes, unsigned long p_x )
{
	unsigned short a, b, c, d;
	unsigned long  y;

	d = (unsigned short)(p_x & 0xFF);
	p_x >>= 8;
	c = (unsigned short)(p_x & 0xFF);
	p_x >>= 8;
	b = (unsigned short)(p_x & 0xFF);
	p_x >>= 8;
	a = (unsigned short)(p_x & 0xFF);
	y = p_PSboxes->S[0][a] + p_PSboxes->S[1][b];
	y = y ^ p_PSboxes->S[2][c];
	y = y + p_PSboxes->S[3][d];

	return y;
}


/***************************************************************************//**
 * @brief Initialise P-array et les S-boxes, pour P-array: XOR avec la key
 * @param[in,out] p_PSboxes	P-array et S-boxes
 * @param[in] p_pass			Clé
 * @param[in] p_passSize		Taille de la clé
 * @return[NONE]
 */
void Crypt::Internal::BLW_initPSboxes( BLW p_PSboxes, const char p_pass[], size_t p_passSize )
{
	size_t i=0, j=0, k=0;
	unsigned long data = 0x00000000;

	//init S boxes with ORIG_S values -> Pi decimal values
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 256; j++)
		p_PSboxes->S[i][j] = ORIG_S[i][j];
	}

	//init P boxes with (ORIG_P XOR Key) values
	j = 0;
	for (i = 0; i < NBR_T + 2; ++i) {
		for (k = 0; k < 4; ++k) {
			data = (data << 8) | p_pass[j];
			j = j + 1;
			if (j >= p_passSize)
			j = 0;
		}
		p_PSboxes->P[i] = ORIG_P[i] ^ data;
	}
}


/***************************************************************************//**
 * @brief Initialisation de P-array et des S-boxies via blowfish une première fois
 * @param[in,out] p_PSboxes	P-array et S-boxes
 * @return[NONE]
 */
void Crypt::Internal::BLW_initPSboxes_FirstBLW( BLW p_PSboxes ) {
	int i, j;
	unsigned long xL = 0x00000000, xR = 0x00000000;

	// Init P-array with initial Blowfish algo
	for (i = 0; i < NBR_T + 2; i += 2) {
		BLW_SubEncrypt(p_PSboxes, &xL, &xR);
		p_PSboxes->P[i] = xL;
		p_PSboxes->P[i + 1] = xR;
	}

	// Init S-boxes with initial Blowfish algo
	for (i = 0; i < 4; ++i) {
		for (j = 0; j < 256; j += 2) {
			BLW_SubEncrypt(p_PSboxes, &xL, &xR);
			p_PSboxes->S[i][j] = xL;
			p_PSboxes->S[i][j + 1] = xR;
		}
	}
}


/***************************************************************************//**
 * @brief 16 tours du shéma de Feistel de l'algo de BlowFish
 * @param[in] p_PSboxes	P-array et S-boxes
 * @param[in,out] p_xL	Bloc de 32 bits (correspond au début de l'algo, à la 1ere
 *						partie du bloc initial de 64 bits)
 * @param[in,out] p_xR	Bloc de 32 bits (correspond au début de l'algo, à la 2-ieme
 *						partie du bloc initial de 64 bits)
 * @return[NONE]
 */
void Crypt::Internal::BLW_SubEncrypt( BLW p_PSboxes, unsigned long* p_xL, unsigned long* p_xR )
{
	unsigned long Xl, Xr, temp;
	short i;

	// Permet de ne pas utiliser la notation avec '*' dans chaque calcul
	Xl = *p_xL;
	Xr = *p_xR;

	// On effectue N tours (16 tours sont recommandés)
	for (i = 0; i < NBR_T; ++i) {
		Xl = Xl ^ p_PSboxes->P[i];
		Xr = BLW_F(p_PSboxes, Xl) ^ Xr;
		// Permutation xL <-> xR
		temp = Xl;
		Xl = Xr;
		Xr = temp;
	}

	// On annule la permutation xL <-> xR qui vient d'être faîte lors du N-ieme tour
	temp = Xl;
	Xl = Xr;
	Xr = temp;
	// Dernieres étapes : on multiplie xR par la (N)-ieme ligne de P (elle n'a pas encore été utilisée)
	Xr = Xr ^ p_PSboxes->P[NBR_T];
	// Dernieres étapes : on multiplie xL par la (N+1)-ieme ligne de P (elle n'a pas encore été utilisée)
	Xl = Xl ^ p_PSboxes->P[NBR_T + 1];

	*p_xL = Xl;
	*p_xR = Xr;

}

/***************************************************************************//**
 * @brief Algorithme de chiffrement BlowFish ECB:
 * Initialisation de P-array et des S-boxes + Shéma de Feistel (Blowfish)
 * @param[in,out] p_stream	Flux de données, contient à la fin les données chiffrées
 * @param[in] p_pass		Clé
 * @param[in] p_passSize	Taille de la clé
 * @return bool				si tout se passe bien -> RETURN true;
 */
bool Crypt::Internal::BLW_encrypt_ECB( CryptoStream::CryptoStream_t* p_stream, const char p_pass[], size_t p_passSize )
{
	// On nous demande de crypter
	cryptoLogF(p_stream, "Encrypt init");

	char buff[8] = {0};
	int i, nbr_read = 0;
	BLW_S PSboxes;
	unsigned long xL, xR;

	// Initialisation des boxes en fonction de la clé
	BLW_initPSboxes(&PSboxes, p_pass, p_passSize);
	BLW_initPSboxes_FirstBLW(&PSboxes);

	// Cryptage des données avec Blowfish
	while( (nbr_read=CryptoStream::read(p_stream, 8, buff)) > 0 )
	{
		// S'il y a un padding on rempli d'une série de '0'
		if( nbr_read < 8 )
			for(i=nbr_read; i<8; i++)
				buff[i]=0;
		// On coupe en deux blocs de 32 bits -> algo Blowfish
		xL = *((unsigned long*)(buff));
		xR = *((unsigned long*)(buff+4));
		BLW_SubEncrypt(&PSboxes, &xL, &xR);
		buff[0] = ((char*)&xL)[0];
		buff[1] = ((char*)&xL)[1];
		buff[2] = ((char*)&xL)[2];
		buff[3] = ((char*)&xL)[3];
		buff[4] = ((char*)&xR)[0];
		buff[5] = ((char*)&xR)[1];
		buff[6] = ((char*)&xR)[2];
		buff[7] = ((char*)&xR)[3];
		// On ecrase le stream d'entree avec le resultat chiffree
		CryptoStream::write(p_stream, 8, buff);
		cryptoLogF(p_stream, "BLW_encrypt = %s",buff);
	}

	// Affiche une erreur dans le GUI
	//throw "J' ai une erreur divine...";
	//...
	return true;
}


/***************************************************************************//**
 * @brief Algorithme de déchiffrement BlowFish ECB:
 * Initialisation de P-array et des S-boxes + Shéma de Feistel (Blowfish)
 * @param[in,out] p_stream	Flux de données, contient à la fin les données déchiffrées
 * @param[in] p_pass		Clé
 * @param[in] p_passSize	Taille de la clé
 * @return bool				si tout se passe bien -> RETURN true;
 */
bool Crypt::Internal::BLW_decrypt_ECB( CryptoStream::CryptoStream_t* p_stream, const char p_pass[], size_t p_passSize )
{
	// On nous demande de decrypter
	cryptoLogF(p_stream, "Decrypt init");

	BLW_S PSboxes;
	char buff[8] = {0};
	int i, nbr_read = 0;
	unsigned long temp, xL, xR;

	// Initialisation des boxes en fonction de la clé
	BLW_initPSboxes(&PSboxes, p_pass, p_passSize);
	BLW_initPSboxes_FirstBLW(&PSboxes);

	// Cryptage des données avec Blowfish
	while( (nbr_read=CryptoStream::read(p_stream, 8, buff)) > 0 )
	{
		// S'il y a un padding on rempli d'une série de '0'
		if( nbr_read < 8 )
			for(i=nbr_read; i<8; i++)
				buff[i]=0;

		// On coupe en deux blocs de 32 bits -> algo Blowfish
		xL = *((unsigned long*)(buff));
		xR = *((unsigned long*)(buff+4));
		// On effectue N tours (16 tours sont recommandés)
		for (i = NBR_T + 1; i > 1; --i) {
			xL = xL ^ PSboxes.P[i];
			xR = BLW_F(&PSboxes, xL) ^ xR;
			// Permutation xL <-> xR
			temp = xL;
			xL = xR;
			xR = temp;
		}
		// On annule la permutation xL <-> xR qui vient d'être faîte lors du N-ieme tour
		temp = xL;
		xL = xR;
		xR = temp;
		// Dernieres étapes : on multiplie xR par la 2-ieme ligne de P (elle n'a pas encore été utilisée)
		xR = xR ^ PSboxes.P[1];
		// Dernieres étapes : on multiplie xL par la 1ere ligne de P (elle n'a pas encore été utilisée)
		xL = xL ^ PSboxes.P[0];

		// On ecrase le stream d'entree avec le resultat chiffree
		buff[0] = ((char*)&xL)[0];
		buff[1] = ((char*)&xL)[1];
		buff[2] = ((char*)&xL)[2];
		buff[3] = ((char*)&xL)[3];
		buff[4] = ((char*)&xR)[0];
		buff[5] = ((char*)&xR)[1];
		buff[6] = ((char*)&xR)[2];
		buff[7] = ((char*)&xR)[3];
		CryptoStream::write(p_stream, 8, buff);
		cryptoLogF(p_stream, "BLW_decrypt = %s",".");
	}

	// Affiche une erreur dans le GUI
	//throw "J' ai une erreur divine...";
	//...
	return true;
}


/***************************************************************************//**
 * @brief Algorithme de chiffrement BlowFish CBC:
 * Initialisation de P-array et des S-boxes + Shéma de Feistel (Blowfish)
 * @param[in,out] p_stream	Flux de données, contient à la fin les données chiffrées
 * @param[in] p_pass		Clé
 * @param[in] p_passSize	Taille de la clé
 * @return bool				si tout se passe bien -> RETURN true;
 */
bool Crypt::Internal::BLW_encrypt_CBC( CryptoStream::CryptoStream_t* p_stream, const char p_pass[], size_t p_passSize, const char p_pass2[], size_t p_passSize2 )
{
	// On nous demande de crypter
	cryptoLogF(p_stream, "Encrypt init");

	char buff[8] = {0};
	int i, nbr_read = 0;
	BLW_S PSboxes;
	unsigned long xL, xR;

	// Initialisation des boxes en fonction de la clé
	BLW_initPSboxes(&PSboxes, p_pass, p_passSize);
	BLW_initPSboxes_FirstBLW(&PSboxes);

	// On init Xl et Xr avec le vecteur d'initialisation
	if( p_passSize2 != 8 )
		throw "Vecteur d'initialisation incorrect\n Attendu : 64 bits / 8 characters";
	xL = *((unsigned long*)(p_pass2));
	xR = *((unsigned long*)(p_pass2+4));
	// On memorise le vecteur d'initialisation
	CryptoStream::write(p_stream, 8, p_pass2);

	// Cryptage des données avec Blowfish
	while( (nbr_read=CryptoStream::read(p_stream, 8, buff)) > 0 )
	{
		// S'il y a un padding on rempli d'une série de '0'
		if( nbr_read < 8 )
			for(i=nbr_read; i<8; i++)
				buff[i]=0;
		// On coupe en deux blocs de 32 bits -> algo Blowfish
		// et on les XOR avec les anciennes valeurs de xL et xR : CBC
		cryptoLogF(p_stream, "ivL=%ld", xL);
		cryptoLogF(p_stream, "ivR=%ld", xR);
		xL ^= *((unsigned long*)(buff));
		xR ^= *((unsigned long*)(buff+4));
		BLW_SubEncrypt(&PSboxes, &xL, &xR);
		buff[0] = ((char*)&xL)[0];
		buff[1] = ((char*)&xL)[1];
		buff[2] = ((char*)&xL)[2];
		buff[3] = ((char*)&xL)[3];
		buff[4] = ((char*)&xR)[0];
		buff[5] = ((char*)&xR)[1];
		buff[6] = ((char*)&xR)[2];
		buff[7] = ((char*)&xR)[3];
		// On ecrase le stream d'entree avec le resultat chiffree
		CryptoStream::write(p_stream, 8, buff);
	}
	return true;
}


/***************************************************************************//**
 * @brief Algorithme de déchiffrement BlowFish CBC:
 * Initialisation de P-array et des S-boxes + Shéma de Feistel (Blowfish)
 * @param[in,out] p_stream	Flux de données, contient à la fin les données déchiffrées
 * @param[in] p_pass		Clé
 * @param[in] p_passSize	Taille de la clé
 * @return bool				si tout se passe bien -> RETURN true;
 */
bool Crypt::Internal::BLW_decrypt_CBC( CryptoStream::CryptoStream_t* p_stream, const char p_pass[], size_t p_passSize )
{
	// On nous demande de decrypter
	cryptoLogF(p_stream, "Decrypt init");

	BLW_S PSboxes;
	char buff[8] = {0};
	char vecInit[8] = {0};
	int i, nbr_read = 0;
	unsigned long temp, xL, xR, last_xL, last_xR, tmp_xL, tmp_xR;

	// Initialisation des boxes en fonction de la clé
	BLW_initPSboxes(&PSboxes, p_pass, p_passSize);
	BLW_initPSboxes_FirstBLW(&PSboxes);

	// On lit les 64 premiers bits du flux chiffré : le vecteur d'initialisation
	if( CryptoStream::read(p_stream, 8, vecInit) < 8 )
		throw "Donnée chiffrée incorrecte\nAttendu : <InitVector(64bit)><EncData>";
	// On init l'ancienne valeur de Xl et Xr avec le vecteur d'initialisation
	last_xL = *((unsigned long*)(vecInit));
	last_xR = *((unsigned long*)(vecInit+4));

	// Cryptage des données avec Blowfish
	while( (nbr_read=CryptoStream::read(p_stream, 8, buff)) > 0 )
	{
		// S'il y a un padding on rempli d'une série de '0'
		if( nbr_read < 8 )
			for(i=nbr_read; i<8; i++)
				buff[i]=0;

		// On coupe en deux blocs de 32 bits -> algo Blowfish
		xL = *((unsigned long*)(buff));
		xR = *((unsigned long*)(buff+4));
		// On effectue N tours (16 tours sont recommandés)
		for (i = NBR_T + 1; i > 1; --i) {
			xL = xL ^ PSboxes.P[i];
			xR = BLW_F(&PSboxes, xL) ^ xR;
			// Permutation xL <-> xR
			temp = xL;
			xL = xR;
			xR = temp;
		}
		// On annule la permutation xL <-> xR qui vient d'être faîte lors du N-ieme tour
		temp = xL;
		xL = xR;
		xR = temp;
		// Dernieres étapes : on multiplie xR par la 2-ieme ligne de P (elle n'a pas encore été utilisée)
		xR = xR ^ PSboxes.P[1];
		// Dernieres étapes : on multiplie xL par la 1ere ligne de P (elle n'a pas encore été utilisée)
		xL = xL ^ PSboxes.P[0];

		// CBC
		// Apres déchiffrement, on XOR avec les 64bits chiffrés précédents
		// (OU si c'est le premier bloc, avec la vecteur d'initialisation)
		tmp_xL = xL;
		tmp_xR = xR;
		cryptoLogF(p_stream, "ivL=%ld, xL=%ld", last_xL, xL);
		cryptoLogF(p_stream, "ivR=%ld, xR=%ld", last_xR, xR);
		xL ^= last_xL;
		xR ^= last_xR;
		last_xL = tmp_xL;
		last_xR = tmp_xR;

		// On ecrase le stream d'entree avec le resultat déchiffré
		buff[0] = ((char*)&xL)[0];
		buff[1] = ((char*)&xL)[1];
		buff[2] = ((char*)&xL)[2];
		buff[3] = ((char*)&xL)[3];
		buff[4] = ((char*)&xR)[0];
		buff[5] = ((char*)&xR)[1];
		buff[6] = ((char*)&xR)[2];
		buff[7] = ((char*)&xR)[3];
		CryptoStream::write(p_stream, 8, buff);
	}

	// Affiche une erreur dans le GUI
	//throw "J' ai une erreur divine...";
	//...
	return true;
}
