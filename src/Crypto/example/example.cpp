#include "example.h"
#include "../Tools.h"
#if 0// Pour désactiver le log
#	undef cryptoLog
#	undef cryptoLogF
#	define cryptoLog(...)
#	define cryptoLogF(...)
#endif


/***************************************************************************//*!
* @brief Permet de crypter des données {stream} avec la clé {pass} qui est de taille
* {passSize}.
* @param[in,out] stream		Les données a crypter. C'est aussi ici que l'on écrit
*							le résultat.
* @param[in] pass			La clé a utiliser.
* @param[in] passSize		La taille de la clé {pass}
* @param[in] isEncryptMod	TRUE si on doit crypter. FALSE si on doit décrypter.
* @return TRUE si tout s'est bien passé. FALSE sinon
*
* @throw Peut lever une exception pour fournir une information sur l'erreur.
*/
bool Crypt::example( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	if( isEncryptMod ){
		cryptoLogF(stream, "Encrypt init");
		// On nous demande de crypter
		char buff;
		int iPass = 0;
		while( CryptoStream::read(stream, 1, &buff) == 1 )
		{
			buff ^= 42;
			buff = Tools::rotateLeft(buff);// Rotation du char ;-)
			buff ^= pass[iPass%passSize];
			CryptoStream::write(stream, 1, &buff);
			cryptoLog(stream, "Message simple printf like %s\n","dieu");
			++iPass;
		}
	}else{
		// On nous demande de décrypter
		char buff;
		while( CryptoStream::read(stream, 1, &buff) == 1 )
		{
			buff ^= 42;
			buff = Tools::rotateRight(buff);// Rotation du char ;-)
			CryptoStream::write(stream, 1, &buff);
		}
	}

	// Affiche une erreur dans le GUI
	//throw "J' ai une erreur divine...";

	return true;
}
