#ifndef EXAMPLE_H
#define EXAMPLE_H

#include "../../Core/CryptoStream.h"

// On met Example dans un namespace pour éviter les collisions avec d'autres fonctions
namespace Crypt {

	bool example( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );

}
#endif // EXAMPLE_H
