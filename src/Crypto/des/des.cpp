#include <stdio.h>
#include <string.h>
#include "des.h"

// http://www.commentcamarche.net/contents/204-introduction-au-chiffrement-avec-des

#define BIT_MASK(n) (1ULL << (n))
#define SET_BIT_N(val,n) ((val) |= BIT_MASK(n))
#define CLR_BIT_N(val,n) ((val) &= ~BIT_MASK(n))
#define TST_BIT_N(val,n) ((val) & BIT_MASK(n))

// TABLES DES

const char PI[] = {
	58, 50, 42, 34, 26, 18, 10, 2,
	60, 52, 44, 36, 28, 20, 12, 4,
	62, 54, 46, 38, 30, 22, 14, 6,
	64, 56, 48, 40, 32, 24, 16, 8,
	57, 49, 41, 33, 25, 17, 9, 1,
	59, 51, 43, 35, 27, 19, 11, 3,
	61, 53, 45, 37, 29, 21, 13, 5,
	63, 55, 47, 39, 31, 23, 15, 7
};

const char E[] = {
	32, 1, 2, 3, 4, 5,
	4, 5, 6, 7, 8, 9,
	8, 9, 10, 11, 12, 13,
	12, 13, 14, 15, 16, 17,
	16, 17, 18, 19, 20, 21,
	20, 21, 22, 23, 24, 25,
	24, 25, 26, 27, 28, 29,
	28, 29, 30, 31, 32, 1
};

const char G_Key[8][64] = {
	{
		14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7,
		0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8,
		4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0,
		15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13
	},
	{
		15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10,
		3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5,
		0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15,
		13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9
	},
	{
		10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8,
		13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1,
		13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7,
		1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12
	},
	{
		7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15,
		13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9,
		10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4,
		3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14
	},
	{
		2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9,
		14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6,
		4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14,
		11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3
	},
	{
		12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11,
		10,15,4,2,7,12,9,5,6,1,12,14,0,11,3,8,
		9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6,
		4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13
	},
	{
		4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1,
		13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6,
		1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2,
		6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12
	},
	{
		13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7,
		1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2,
		7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8,
		2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11
	}
};

const char P[] = {
	16, 7, 20, 21, 29, 12, 28, 17,
	1, 15, 23, 26, 5, 18, 31, 10,
	2, 8, 24, 14, 32, 27, 3, 9,
	19, 13, 30, 6, 22, 11, 4, 25
};

const char P_INVERSE[] = {
	40, 8, 48, 16, 56, 24, 64, 32,
	39, 7, 47, 15, 55, 23, 63, 31,
	38, 6, 46, 14, 54, 22, 62, 30,
	37, 5, 45, 13, 53, 21, 61, 29,
	36, 4, 44, 12, 52, 20, 60, 28,
	35, 3, 43, 11, 51, 19, 59, 27,
	34, 2, 42, 10, 50, 18, 58, 26,
	33, 1, 41, 9, 49, 17, 57, 25
};

// TABLES GENERATION CLES

const char CP1[] = {
	57, 49, 41, 33, 25, 17, 9,
	1, 58, 50, 42, 34, 26, 18,
	10, 2, 59, 51, 43, 35, 27,
	19, 11, 3, 60, 52, 44, 36,
	63, 55, 47, 39, 31, 23, 15,
	7, 62, 54, 46, 38, 30, 22,
	14, 6, 61, 53, 45, 37, 29,
	21, 13, 5, 28, 20, 12, 4
};

char CP2[] = {
	14, 17, 11, 24, 1, 5,
	3, 28, 15, 6, 21, 10,
	23, 19, 12 ,4, 26, 8,
	16, 7, 27, 20, 13, 2,
	41, 52, 31, 37, 47, 55,
	30, 40, 51, 45, 33, 48,
	44, 49, 39, 56, 34, 53,
	46, 42, 50, 36, 29, 32
};

// GLOBAL

void print_n_bits(const char* s, TYPE val, int n)
{
	Q_UNUSED(s);
	Q_UNUSED(val);
	signed int i;

	// fprint(fp, "%s\t(%d):\t", s, n);

	for (i = n-1; i >= 0; i--) {
		// fprint(fp, "%s", TST_BIT_N(val, i) ? "1" : "0");
	}

	// fprint(fp, "\n");
}

TYPE char8_to_TYPE(const char* char8)
{
	TYPE out = 0;

	memcpy(&out, char8, sizeof(out));

	// fprint(fp, "hex:\t\t%#llx\n", out);
	print_n_bits("bin", out, 64);

	return out;
}

TYPE process_table(TYPE in, const char* table, unsigned int start, unsigned int end)
{
	TYPE out = 0;
	unsigned int i;

	for (i = start; i < end; i++)
	{
		if (TST_BIT_N(in, table[i]-1))
			SET_BIT_N(out, i-start);
	}

	return out;
}

TYPE fusion(TYPE gauche, TYPE droite, char n)
{
	return ((gauche << n) + droite);
}

// DES

TYPE substitution(TYPE bloc48)
{
	TYPE bloc32 = 0;
	char ligne, colonne, val;
	int i, j;

	j = 0;
	for (i = 0; i < 48/6; i++)
	{
		ligne = 0;
		colonne = 0;

		if (TST_BIT_N(bloc48, i*6)) SET_BIT_N(ligne, 0);
		if (TST_BIT_N(bloc48, i*6+5)) SET_BIT_N(ligne, 1);

		if (TST_BIT_N(bloc48, i*6+1)) SET_BIT_N(colonne, 0);
		if (TST_BIT_N(bloc48, i*6+2)) SET_BIT_N(colonne, 1);
		if (TST_BIT_N(bloc48, i*6+3)) SET_BIT_N(colonne, 2);
		if (TST_BIT_N(bloc48, i*6+4)) SET_BIT_N(colonne, 3);

		val = G_Key[i][ligne*16+colonne];

		if (TST_BIT_N(val, 0)) SET_BIT_N(bloc32, j);
		if (TST_BIT_N(val, 1)) SET_BIT_N(bloc32, j+1);
		if (TST_BIT_N(val, 2)) SET_BIT_N(bloc32, j+2);
		if (TST_BIT_N(val, 3)) SET_BIT_N(bloc32, j+3);

		j += 4;
	}

	return bloc32;
}

TYPE do_stuff_bloc(unsigned char encrypt, const char* txt_bloc, TYPE* cles)
{
	TYPE bloc;
	TYPE bloc_gauche, bloc_droite;
	TYPE bloc_droite_init;
	unsigned int i;

	bloc = char8_to_TYPE(txt_bloc);

	// PERMUTATION INITIALE
	// fprint(fp, "\nPERMUTATION INITIALE\n");
	bloc_gauche = process_table(bloc, PI, 0, 32);
	bloc_droite = process_table(bloc, PI, 32, 64);

	print_n_bits("bloc", bloc, 64);
	print_n_bits("gauche", bloc_gauche, 32);
	print_n_bits("droite", bloc_droite, 32);

	// RONDES
	for (i = 0; i < 16; i++)
	{
		// fprint(fp, "\nRONDE %u\n", i+1);

		bloc_droite_init = bloc_droite;

		// EXPANSION
		bloc_droite = process_table(bloc_droite, E, 0, sizeof(E));
		print_n_bits("EXPANSION", bloc_droite, 48);

		// XOR AVEC K
		bloc_droite ^= encrypt ? cles[i] : cles[15-i];
		print_n_bits("XOR AVEC K", bloc_droite, 48);

		// SUBSTITUTION
		bloc_droite = substitution(bloc_droite);
		print_n_bits("SUBSTITUTION", bloc_droite, 32);

		// PERMUTATION
		bloc_droite = process_table(bloc_droite, P, 0, sizeof(P));
		print_n_bits("PERMUTATION", bloc_droite, 32);

		// XOR AVEC G
		bloc_droite ^= bloc_gauche;
		print_n_bits("XOR AVEC G", bloc_droite, 32);

		// Gi+1 = Di
		bloc_gauche = bloc_droite_init;
		print_n_bits("BLOC GAUCHE", bloc_gauche, 32);
	}

	// FUSION
	bloc = fusion(bloc_gauche, bloc_droite, 32);
	print_n_bits("\nFUSION\t", bloc, 64);

	// PERMUTATION INVERSE
	bloc = process_table(bloc, P_INVERSE, 0, sizeof(P_INVERSE));
	print_n_bits("\nPERMUT INVERSE", bloc, 64);

	// RESULTAT
	// fprint(fp, "\nRESULTAT:\t\t%#llx -> %#llx\n", bloc_init, bloc);

	return bloc;
}

void do_stuff(unsigned char encrypt, CryptoStream::CryptoStream_t* stream, TYPE* cles, const char* infile, const char* outfile)
{
	FILE *fp_in=0, *fp_out=0;
	enum {BUFFER_SIZE = 9};
	char buffer[BUFFER_SIZE] = {0};
	int readSize;
	TYPE bloc;
	unsigned int i = 1;
	unsigned char padding_byte;

	if (infile)
	{
		fp_in = fopen(infile, "rb");
		if (fp_in == NULL) {
			perror("fopen");
			exit(1);
		}
	}

	if (outfile)
	{
		fp_out = fopen(outfile, "wb+");
		if (fp_out == NULL) {
			perror("fopen");
			exit(1);
		}
	}

	while (1)
	{
		readSize = infile ? fread(buffer, sizeof(char), 8, fp_in) : CryptoStream::read(stream, 8, buffer);
		if (readSize != 8)
			break;

		bloc = do_stuff_bloc(encrypt, buffer, cles);
		memcpy(buffer, &bloc, BUFFER_SIZE);

		if (outfile)
			fwrite(buffer, sizeof(char), 8, fp_out);
		else
			CryptoStream::write(stream, 8, buffer);

		i++;
	}

	if (!readSize)
		goto thierno_coding_style;// [I-PC] Serieux !?? Wtf

	if (encrypt)
	{
		// fprint(fp, "BLOC %u", i);

		// padding
		memset(buffer+readSize, 8-readSize, 8-readSize);
		bloc = do_stuff_bloc(encrypt, buffer, cles);
		memcpy(buffer, &bloc, BUFFER_SIZE);

		if (outfile)
			fwrite(buffer, sizeof(char), 8, fp_out);
		else
			CryptoStream::write(stream, BUFFER_SIZE-1, buffer);
	}
	else
	{
		// fprint(fp, "BLOC %u", i);

		bloc = do_stuff_bloc(encrypt, buffer, cles);
		memcpy(buffer, &bloc, BUFFER_SIZE);

		// padding
		if( buffer[7] > 0 && buffer[7] < 8 )
		{
			padding_byte = buffer[7];

			for (i = 0; i < padding_byte; i++)
				buffer[7-i] = '\0';
		}

		if (outfile)
			fwrite(buffer, sizeof(char), 8, fp_out);
		else
			CryptoStream::write(stream, 8, buffer);
	}

// [I-PC] Serieux !?? Wtf
thierno_coding_style: // epic troll :]
	if (infile)
		fclose(fp_in);

	if (outfile)
		fclose(fp_out);
}

void chiffrer(CryptoStream::CryptoStream_t* stream, TYPE* cles, const char* infile, const char* outfile)
{
	do_stuff(1, stream, cles, infile, outfile);
}

void dechiffrer(CryptoStream::CryptoStream_t* stream, TYPE* cles, const char* infile, const char* outfile)
{
	do_stuff(0, stream, cles, infile, outfile);
}

// GENERATIONS CLES

TYPE supprimer_bits_parite(TYPE cle64)
{
	TYPE cle56 = 0;
	unsigned int i, j, k;

	k = 0;
	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 7; j++)
		{
			if (TST_BIT_N(cle64, i*8+j))
				SET_BIT_N(cle56, k);
			k++;
		}
	}

	return cle56;
}

TYPE rotation_circulaire_gauche(TYPE cle28, char shift)
{
	return ((cle28 << shift) | (cle28 >> (28 - shift))) & 0xfffffff;
}

void generer_cles(const char* cle64, TYPE* cles)
{
	TYPE cle;
	TYPE cle_gauche_init, cle_droite_init;
	TYPE cle_gauche, cle_droite;
	unsigned char ls[] = {1, 2, 4, 6, 8, 10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28};
	unsigned int i;

	// fprint(fp, "#\n# GENERATION DES CLES\n#\n\n");

	cle = char8_to_TYPE(cle64);

	// SUPPRESSION DES BITS DE PARITE
	// fprint(fp, "\nSUPPRESSION DES BITS DE PARITE\n");
	cle = supprimer_bits_parite(cle);
	print_n_bits("cle\t", cle, 56);

	// PERMUTATION INITIALE
	// fprint(fp, "\nPERMUTATION INITIALE\n");
	cle_gauche_init = process_table(cle, CP1, 0, 28);
	cle_droite_init = process_table(cle, CP1, 28, 56);

	print_n_bits("cle\t", cle, 56);
	print_n_bits("gauche\t", cle_gauche_init, 28);
	print_n_bits("droite\t", cle_droite_init, 28);

	// ITERATIONS
	for (i = 0; i < 16; i++)
	{
		// fprint(fp, "\nCLE %u\n", i+1);

		// ROTATION CIRCULAIRE GAUCHE
		// fprint(fp, "ROTATION CIRCULAIRE GAUCHE\n");
		cle_gauche = rotation_circulaire_gauche(cle_gauche_init, ls[i]);
		cle_droite = rotation_circulaire_gauche(cle_droite_init, ls[i]);
		print_n_bits("\tgauche", cle_gauche, 28);
		print_n_bits("\tdroite", cle_droite, 28);

		// FUSION
		cle = fusion(cle_gauche, cle_droite, 28);
		print_n_bits("FUSION\t", cle, 56);

		// PERMUTATION 2
		cle = process_table(cle, CP2, 0, sizeof(CP2));
		print_n_bits("PERMUTATION 2", cle, 56);

		// RESULTAT
		// fprint(fp, "RESULTAT:\t\t%#llx\n", cle);

		cles[i] = cle;
	}
}

//

bool Crypt::des(CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	Q_UNUSED(passSize);
	TYPE cles[16] = {0};

	generer_cles(pass, cles);

	if (isEncryptMod)
		chiffrer(stream, cles, NULL, NULL);
	else
		dechiffrer(stream, cles, NULL, NULL);

	return true;
}
