#ifndef DES_H
#define DES_H

#include "../../Core/CryptoStream.h"

typedef unsigned long long  TYPE;

void generer_cles(const char* cle64, TYPE* cles);

void chiffrer(CryptoStream::CryptoStream_t* stream, TYPE* cles, const char* infile, const char* outfile);
void dechiffrer(CryptoStream::CryptoStream_t* stream, TYPE* cles, const char* infile, const char* outfile);

namespace Crypt {
	bool des( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
}

#endif // DES_H
