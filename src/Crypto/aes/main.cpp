#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aes.h"


int main()
{
	unsigned char buf[16];
	int n,i;
	char* key	= "000102030405060708090a0B0C0D0E0F01112131415161718191A1B1C1D1E1F1";
	char * iv	= "00000000000000000000000000000000";
	FILE *ptrr, *ptrw;
	ptrr = fopen("test.txt", "rb");
	ptrw = fopen("test.enc", "wb");
	
	init(AES_256, CBC, iv, key);
	memset(buf, 0, sizeof(buf));
	while(1)
	{
		n = fread(buf, sizeof(buf), 1, ptrr);
		if(strlen(buf) == 0)break;
		fwrite(enc(buf), sizeof(buf), 1, ptrw);
		if(!n)break;
		memset(buf, 0, sizeof(buf));
	}
	fclose(ptrr);fclose(ptrw);
		
	ptrr = fopen("test.enc", "rb");
	ptrw = fopen("test.dec", "wb");
	init(AES_256, CBC, iv, key);
	while(1)
	{
		n = fread(buf, sizeof(buf), 1, ptrr);
		if(n==0) break;
		fwrite(dec(buf), sizeof(buf), 1, ptrw);
	}
	fclose(ptrr);fclose(ptrw);
	clean_aes();
}
