#ifndef AES_CORE_H
#define AES_CORE_H

#define AES_128 1
#define AES_192 2
#define AES_256 3


void encrypt(unsigned char state[4][4]);
void decrypt(unsigned char state[4][4]);
int setup(int key_size, unsigned char* key);
void clean();

#endif

#ifndef SUCCESS
#define SUCCESS 1
#endif

#ifndef FAIL
#define FAIL -1
#endif
