#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aes.h"
#include <QObject>
#include <QMessageBox>

int aes_mode = ECB;
unsigned char ci_1[4][4];
unsigned char state[4][4];
unsigned char ciphered[16];
unsigned char plain[16];
int i,j;
int status;

/*
	@Brief conversion d'un chaine de hex en matrice  hex de 4x4
	@param[in] str chaine à convertir en matrice
	@param[in] state matrice hex 4x4 résultante
*/
static int strHexToMat(unsigned char * str, unsigned char state[4][4])
{
	int val;
	char tmp[3] = "00";
	if(strlen((const char *)str) != 32)
	{
		printf("input size must be 16 bytes, found:%i\n", strlen((const char *)str));
		return FAIL;
	}
	for(i=0; i<32; i+=2)
	{
		strncpy(tmp, (char *)str+i, 2);
		sscanf(tmp, "%x", &val);
		state[(i/2)%4][(i/2)/4] = val;
		/*
			le remplissage de la matrice se fait colonne par colonne
		*/
	}
	return SUCCESS;
}

static int bufToMat(unsigned char buf[16], unsigned char state[4][4])
{
	for(i=0; i<4; i++)
		for(j=0; j<4; j++)
			state[j][i] = buf[i*4+j]; //remplissage par colonne
	return SUCCESS;
}


/*
	@Brief conversion d'une matrice  hex de 4x4 en chaine de hex
	@param[in] state matrice hex 4x4 à convertir
	@param[in] str chaine de hex résultante
*/
static int matToBuf(unsigned char state[4][4], unsigned char buf[16])
{
	for(i=0; i<4; i++)
		for(j=0; j<4; j++)
			buf[i*4+j] = state[j][i];
	return SUCCESS;
}

/*
	@brief Initialisation du core AES
	@param[in] key_size taille de clé (AES_128 | AES_192 | AES_256)
	@param[in] mode mode de chainage (ECB | CBC)
	@param[in] init_vect vecteur d'initialisation (dans le cas du mode CBC)
	@param[in] key clé de chiffrément
	@param[out] status 'SUCCESS | FAIL)

*/
int init(int key_size, int mode, const char *init_vect, unsigned char* key)
{
	if((mode != ECB && mode != CBC) || (key_size != AES_128 && key_size != AES_192 && key_size != AES_256))
	{
		return FAIL;
	}
	aes_mode = mode;

	if(!setup(key_size, key))
	{
		return FAIL;
	}

	strHexToMat((unsigned char *)init_vect, ci_1);
	printf("%s-%s\niv : %s\nkey: %s\n",
			key_size==AES_128?"AES_128":key_size==AES_192?"AES_192":"AES_256",mode==ECB?"ECB":"CBC",
			init_vect, key);

	return SUCCESS;
}

/*
	@brief padding selon PKCS#7 [mode par defaut dans openssl]
*/
static void padding(unsigned char msg[16])
{
	unsigned char pad = 16 - strlen((const char *)msg);

	for(i=strlen((const char *)msg); i<16; i++)
	{
		msg[i] = pad;
	}
}
/*
 * @brief   detcter et supprimer le padding ajouter lors du chiffrement
 * @param[in] msg   bloc contenant éventuellement un padding
 */
static unsigned char rm_padding(unsigned char msg[16])
{
	int i;
	unsigned char pad = msg[15];
	if(pad > 15)
		return 0;
	for(i=1; i< pad; i++)
	{
		if(msg[15-i] != pad)
			return 0;
	}
	return pad;
}
/*
	@brief Routine de chiffrement
	@param[in] msg message claire à chiffrer
	@param[out] ciphered pointeur sur le message chiffré
*/
unsigned char* enc(unsigned char msg[16])
{
	if(strlen((const char *)msg) < 16)
	{
		padding(msg);
	}
	bufToMat(msg, state);
	if(aes_mode == CBC)
	{
		for(i=0; i<4; i++)
			for(j=0; j<4; j++)
				state[i][j] = state[i][j] ^ ci_1[i][j];
	}
	encrypt(state);

	if(aes_mode == CBC)
	{
		for(i=0; i<4; i++)
			for(j=0; j<4; j++)
				ci_1[i][j] = state[i][j];
	}
	matToBuf(state, ciphered);
	return ciphered;
}

/*
	@brief Routine de déchiffrement
	@param[in] msg message chiffré à déchiffrer
	@param[out] msg pointeur sur le message déchiffré
*/
unsigned char* dec(unsigned char msg[16])
{
	bufToMat(msg, state);
	decrypt(state);
	if(aes_mode == CBC)
	{
		for(i=0; i<4; i++)
			for(j=0; j<4; j++)
				state[i][j] = state[i][j] ^ ci_1[i][j];
		bufToMat(msg, ci_1);
	}
	matToBuf(state, plain);
	return plain;
}

/*
	@brief	Libération de la mémoire occupée par AES
*/
void clean_aes(void)
{
	clean();
	return;
}

/*
 * @brief   lecture et chiffrement/déchiffrement bloc par bloc du fichier entré
 *          puis écriture dans le fichier sortie.
 *
 */
static void aes_runner(CryptoStream::CryptoStream_t *stream, bool isEncryptMod)
{
	int nb_blocs;
	unsigned char buf[16];
	unsigned char * tmp;
	int i;
	unsigned char pad;

	nb_blocs = stream->m_readerSize/16 + (stream->m_readerSize%16 ? 1 : 0);
	for(i=0; i<nb_blocs-1; i++)
	{
		CryptoStream::read(stream, 16, (char*)buf);
		CryptoStream::write(stream, 16,isEncryptMod?(char*)enc(buf):(char*)dec(buf));
	}
	memset(buf, 0, 16);
	if(isEncryptMod)
	{
		CryptoStream::read(stream, 16, (char*)buf);
		padding(buf);
		CryptoStream::write(stream, 16, (char*)enc(buf));
	}
	else
	{
		CryptoStream::read(stream, 16, (char*)buf);
		tmp = dec(buf);
		pad = rm_padding(tmp);
		CryptoStream::write(stream, 16-pad, (char*)tmp);
	}
}
/*
 *  @brief  fonctions appelé depuis le GUI de Guillaume ;)
 */
bool Crypt::aes_128_ECB(CryptoStream::CryptoStream_t *stream, const char pass[], size_t passSize, bool isEncryptMod)
{
	if(passSize != 32)
	{
		throw ("Invalid key for AES-128\nkey size must be 32 hexadecimal digits (16 bytes) (32 char required != actual "+QString::number(passSize)+")").toUtf8().data();
	}

	status = init(AES_128, ECB, "00000000000000000000000000000000", (unsigned char *)pass);
	if(status == FAIL)
	{
		throw ("Cannot initialize AES\nInvalid Parameters");
	}
	aes_runner(stream, isEncryptMod);
	clean_aes();
	return true;
}

bool Crypt::aes_192_ECB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	if(passSize != 48)
	{
		throw ("Invalid key for AES-192\nkey size must be 48 hexadecimal digits (24 bytes) (48 char required != actual "+QString::number(passSize)+")").toUtf8().data();
	}
	status = init(AES_192, ECB, "00000000000000000000000000000000", (unsigned char *)pass);
	if(status == FAIL)
	{
		throw ("Cannot initialize AES\nInvalid Parameters");
	}
	aes_runner(stream, isEncryptMod);
	clean_aes();
	return true;
}

bool Crypt::aes_256_ECB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	if(passSize != 64)
	{
		throw ("Invalid key for AES-256\nkey size must be 64 hexadecimal digits (32 bytes) (64 char required != actual "+QString::number(passSize)+")").toUtf8().data();
	}
	status = init(AES_256, ECB, "00000000000000000000000000000000", (unsigned char *)pass);
	if(status == FAIL)
	{
		throw ("Cannot initialize AES\nInvalid Parameters");
	}
	aes_runner(stream, isEncryptMod);
	clean_aes();
	return true;
}

bool Crypt::aes_128_CBC( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	if(passSize != 32)
	{
		throw ("Invalid key for AES-128\nkey size must be 32 hexadecimal digits (16 bytes) (32 char required != actual "+QString::number(passSize)+")").toUtf8().data();
	}
	status = init(AES_128, CBC, "00000000000000000000000000000000", (unsigned char *)pass);
	if(status == FAIL)
	{
		throw ("Cannot initialize AES\nInvalid Parameters");
	}
	aes_runner(stream, isEncryptMod);
	clean_aes();
	return true;
}

bool Crypt::aes_192_CBC( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	if(passSize != 48)
	{
		throw ("Invalid key for AES-192\nkey size must be 48 hexadecimal digits (24 bytes) (48 char required != actual "+QString::number(passSize)+")").toUtf8().data();
	}
	status = init(AES_192, CBC, "00000000000000000000000000000000", (unsigned char *)pass);
	if(status == FAIL)
	{
		throw ("Cannot initialize AES\nInvalid Parameters");
	}

	aes_runner(stream, isEncryptMod);
	clean_aes();
	return true;
}

bool Crypt::aes_256_CBC( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod )
{
	if(passSize != 64)
	{
		throw ("Invalid key for AES-256\nkey size must be 64 hexadecimal digits (32 bytes) (64 char required != actual "+QString::number(passSize)+")").toUtf8().data();
	}
	status = init(AES_256, CBC, "00000000000000000000000000000000", (unsigned char *)pass);
	if(status == FAIL)
	{
		throw ("Cannot initialize AES\nInvalid Parameters");
		return false;
	}
	aes_runner(stream, isEncryptMod);
	clean_aes();
	return true;
}

