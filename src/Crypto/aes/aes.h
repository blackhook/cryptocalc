#ifndef AES_H
#define AES_H

#define ECB	1
#define CBC 2

#include "aes_core.h"
#include "../../Core/CryptoStream.h"


/*
	@brief Initialisation du core AES
	@param[in] key_size taille de clé (AES_128 | AES_192 | AES_256)
	@param[in] mode mode de chainage (ECB | CBC)
	@param[in] init_vect vecteur d'initialisation (dans le cas du mode CBC)
	@param[in] key clé de chiffrément
	@param[out] status 'SUCCESS | FAIL)
*/

int init(int key_size, int mode, const char * init_vect, unsigned char* key);

/*
	@brief Routine de chiffrement
	@param[in] msg message claire à chiffrer
	@param[out] ciphered pointeur sur le message chiffré
*/
unsigned char* enc(unsigned char* msg);

/*
	@brief Routine de déchiffrement
	@param[in] msg message chiffré à déchiffrer
	@param[out] msg pointeur sur le message déchiffré
*/
unsigned char* dec(unsigned char* msg);

/*
	@brief	Libération de la mémoire occupée par AES
*/
void clean_aes(void);

namespace Crypt {

    bool aes_128_ECB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
    bool aes_192_ECB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
    bool aes_256_ECB( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
    bool aes_128_CBC( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
    bool aes_192_CBC( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
    bool aes_256_CBC( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );

}

#endif
