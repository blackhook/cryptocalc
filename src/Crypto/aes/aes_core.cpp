
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "aes_core.h"


int	NK			=  4;		  //nombre de blocs de 4 octets de la cle
int	NR			= 10;		  //nombre de tours. 10 pour une cle de 128bits
char ENC		=  1;			//encryption
char DEC		= -1; 			//decryption

unsigned char ** keys;

/*
La boite-S: utilisée pour réaliser l'opération "SubByte" lors du chiffrement
pour des raisons d'éfficacité, on pré-calcul les valeurs à l'avance
*/
const unsigned char SBox[16][16] =
{
	{0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76},
	{0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0},
	{0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15},
	{0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75},
	{0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84},
	{0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF},
	{0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8},
	{0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2},
	{0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73},
	{0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB},
	{0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79},
	{0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08},
	{0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A},
	{0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E},
	{0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF},
	{0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16}
};
/* la boite-S inverse: utilisé pour réaliser l'opération "SubByte" inverse lors du déchiffrement*/
const unsigned char InvSBox[16][16] =
{
	{0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB},
	{0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB},
	{0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E},
	{0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25},
	{0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92},
	{0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84},
	{0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06},
	{0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B},
	{0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73},
	{0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E},
	{0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B},
	{0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4},
	{0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F},
	{0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF},
	{0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61},
	{0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D}
};

/*
matrice de l'opération mix_column
*/
const unsigned char mat_mixColumns[4][4] =
{
	{02, 03, 01, 01},
	{01, 02, 03, 01},
	{01, 01, 02, 03},
	{03, 01, 01, 02}
};


const unsigned char rcon[16] =
{
0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a
};

const unsigned char Inv_mat_mixColumns[4][4] =
{
	{0x0e, 0x0b, 0x0d, 0x09},
	{0x09, 0x0e, 0x0b, 0x0d},
	{0x0d, 0x09, 0x0e, 0x0b},
	{0x0b, 0x0d, 0x09, 0x0e}
};



/*
*@Param[op] :ENC/DEC pour utiliser resp. la table SBox/InvSBox
*@Detail extration de l'octet correspondant de Param[byte] dans la SBox/InvSBox
*/
unsigned char get_sub_byte(unsigned char byte, char op)
{
	unsigned char i, j;
	i = byte >> 4;		//4 premiers bits
	j = byte%16;		//4 derniers bits
	return op == ENC ? SBox[i][j] : InvSBox[i][j];
}

/*
*@Detail opération SUB_BYTES
*/
void sub_bytes(unsigned char state[4][4], char op)
{
	int i, j;
	for(i=0; i<4; i++)
		for(j=0; j<4; j++)
			state[i][j] = get_sub_byte(state[i][j], op);
	return;
}
/*
	décalage d'un rang vers la gauche/droite selon
	qu'on soit en chiffrement/déchiffrement
	defini par le parmètre op
*/
void shift_row(unsigned char state[4], char op)
{
	unsigned char tmp;
	if(op == ENC)
	{
		tmp = state[0];
		state[0] = state[1];
		state[1] = state[2];
		state[2] = state[3];
		state[3] = tmp;
	}
	else
	{
		tmp = state[3];
		state[3] = state[2];
		state[2] = state[1];
		state[1] = state[0];
		state[0] = tmp;
	}
	return;
}


/*
* @Detail: Opération Shift Rows
*/
void shift_rows(unsigned char state[4][4], char op)
{
	int i, j;
	for(i=1; i<4; i++)
	{
		for(j=0; j<i; j++)
			shift_row(state[i], op);
	}
	return;
}

//multiplication modulaire a*b
unsigned char mult(unsigned char a, unsigned char b)
{
	int bin_b[5] = {0,0,0,0,0};
	int p = 0, intemp;
	unsigned char temp[5];
	temp[0] = b;

	do
	{
		if (temp[0] % 2 != 0)
		{
			temp[0] -= 1;
			bin_b[p] = 1;
		}
		p++;
		temp[0] /= 2;
	}
	while (temp[0] > 0);
	//bb: contient l'écriture binaire de b
	temp[0] = a;
	temp[1] = a;

	for(p = 1; p < 5; p++)	//on considére b de la forme 0x ou 1x
	{
		intemp = temp[p] * 2;
		if (intemp > 255) temp[p] = (intemp - 256) ^ 27;
		else temp[p] = intemp;
		if (p < 4) temp[p + 1] = temp[p];
	}

	for(p = 0; p < 5; p++)
	{
		if (bin_b[p] == 0) temp[p] = 0;
	}
	return temp[0] ^ temp[1] ^ temp[2] ^ temp[3] ^ temp[4];
	}

/*
*@Detail: Multiplication vectorielle
*/
unsigned char multiply(unsigned char vect1[4], const unsigned char vect2[4])
{
	int i;
	unsigned char res,tmp;
	res = mult(vect1[0], vect2[0]);
	for(i=1; i<4; i++)
	{
		tmp = mult(vect1[i], vect2[i]);
		res = res ^ tmp;
	}
	return res;
}

void printByte(unsigned char byte[4])
{
	int i;
	for(i=0; i<4; i++)	printf("%#02x ",byte[i]);
}

void printMat(unsigned char state[4][4])
{
	int i;
	for(i=0; i<4; i++)
	{
		printByte(state[i]);
		printf("\n");
	}
}

/*
*@Detail: Opération Mix Column
*/
void mix_columns(unsigned char state[4][4], char op)
{
	//printf("State:\n");
	//printMat(state);

	int i,j;
	unsigned char tmp[4][4];
	for(i=0; i<4; i++)
		for(j=0; j<4; j++)
			tmp[j][i] = state[i][j]; // on renverse la matrice pour la manier facilement

	//printf("\nRState:\n");
	//printMat(tmp);

	for(j=0; j<4; j++)
		for(i=0; i<4; i++){// multiplication de deux vecteur {x1,x2,x3,x4} x {y1,y2,y3,y4}
			state[i][j] = op == ENC ? multiply(tmp[j], mat_mixColumns[i]) : multiply(tmp[j], Inv_mat_mixColumns[i]);
			//printf("state[%d][%d] =	",i,j);
			//printByte(tmp[j]);printf(" x ");printByte(mat_mixColumns[i]);printf("\n");
		}
	return;
}

//modifiée
void add_round_key(unsigned char state[4][4], unsigned char round_key[4][4])
{
	int i,j;
	for(j=0; j<4; j++)
		for(i=0; i<4; i++)
			state[i][j] ^= round_key[j][i];
}


void sub_word(unsigned char byte[4])
{
	int i;
	for(i=0; i<4; i++)
		byte[i] = get_sub_byte(byte[i], ENC);
}


void printState(unsigned char byte[4][4])
{
	int i,j;
	for(j=0; j<4; j++)
		for(i=0; i<4; i++)
			printf("%02x",byte[i][j]);
	printf("\n");
}
void printKey(unsigned char byte[4][4])
{
	int i,j;
	for(j=0; j<4; j++)
		for(i=0; i<4; i++)
			printf("%02x",byte[j][i]);
	printf("\n");
}

void printMasterKey(unsigned char byte[][4])
{
	int i,j;
	for(j=0; j<NK; j++)
		for(i=0; i<4; i++)
			printf("%02x",byte[j][i]);
	printf("\n");
}
/************************************************************
@brief	génère a partir de la clé de base, une suite de clés
		(NR+1) clés. chaque round utilise une clé différente
@param[in,out]	emplacement des clés générées
*/
int key_expension(unsigned char key[][4])
{
	int i,t;
	unsigned char tmp[4];
	/*
		allocation mémoire pour la tables keys
		qui contiendra l'ensemble des clés de tours
	*/
	keys = (unsigned char **)malloc(sizeof(unsigned char*)*4*(NR+1));
	if(keys == NULL)
		return FAIL;
	for(i=0; i<4*(NR+1); i++)
	{
		keys[i] = (unsigned char*)malloc(sizeof(unsigned char)*4);
		if(keys[i] == NULL)
			return FAIL;
	}

	for(i=0; i<NK; i++)
		for(t=0; t<4; t++)
			keys[i][t] = key[i][t];

	for(i=NK; i<4*(NR+1); i++)
	{
		for(t=0; t<4; t++)
			tmp[t] = keys[i-1][t];
		if(i % NK ==0)
		{
			shift_row(tmp, ENC);
			sub_word(tmp);
			tmp[0] = tmp[0] ^ rcon[i/NK];

		}
		if((NK > 6) && (i % NK == 4))
			sub_word(tmp);
		for(t=0; t<4; t++)
			keys[i][t] = keys[i-NK][t]^tmp[t];

	}
	return SUCCESS;
}

/*
	@parma libération de mémoire occupée par les clés de tour
*/
void clean()
{
	int i;
	for(i=0; i<4*(NR+1); i++)
		free(keys[i]);
	free(keys);
	printf("\n** memory cleaned **\n");
}
/*************************************************************************
@brief	extrait la clé de round	correspondante apartir de la table keys
@param[in] numero du round
param[in,out] la clé correspondante
*/
void select_round_key(int round, unsigned char key[4][4])
{
	int i,j,t;
	j = 0;
	for(i=round*4; i<4*(round+1); i++)
	{
		for(t=0; t<4; t++)
			key[j][t] = keys[i][t];
		j++;
	}
}
/**************************************************************************
@brief Routine de déchiffrement
@parm[in] state		les octets à déchiffrer sous forme de matrice 4x4
@detail		peut être vu comme un chiffrement inversé (partir de la fin)
			c-a-d elle utilise les meme opération mais dans l'ordre inverse
			et elle utilise les matrices Inverses de celles utilisées lors
			du chiffrement (InvSBox, Inv_mat_mixColumns)
			elle part de la dernière clé (keys[NR]) pour remonter à keys[0].
*/
void decrypt(unsigned char state[4][4])
{
	int r;
	unsigned char roundkey[4][4];
	select_round_key(NR, roundkey);
	//printf("\tinput = ");
	//printState(state);

	add_round_key(state, roundkey);
	for(r=NR-1; r>0; r--)
	{
		shift_rows(state, DEC);
		sub_bytes(state, DEC);

		select_round_key(r,roundkey);

		add_round_key(state,  roundkey);

		mix_columns(state, DEC);
	}
	shift_rows(state, DEC);
	sub_bytes(state, DEC);

	select_round_key(r,roundkey);
	add_round_key(state,  roundkey);

	//printf("\toutput = ");
	//printState(state);
}



/*********************************************************************
@brief	fonction de chiffrement
*/
void encrypt(unsigned char state[4][4])
{
	int r;
	unsigned char roundkey[4][4];
	select_round_key(0,roundkey);
	//printf("\tinput: ");
	//printf("R[00].input = ");
	//printState(state);
	//printf("R[00].k_sch = ");
	//printKey(roundkey);

	add_round_key(state,  roundkey);
	for(r=1; r<NR; r++)
	{
		//printf("R[%02d].start = ",r);
		//printState(state);

		sub_bytes(state, ENC);

		//printf("R[%02d].s_box = ",r);
		//printState(state);

		shift_rows(state, ENC);

		//printf("R[%02d].s_row = ",r);
		//printState(state);

		mix_columns(state, ENC);

		//printf("R[%02d].mxcol = ",r);
		//printState(state);

		select_round_key(r,roundkey);

		//printf("R[%02d].k_sch = ",r);
		//printKey(roundkey);

		add_round_key(state,  roundkey);
	}
	//printf("** Final Round **\n");
	sub_bytes(state, ENC);

	//printf("R[%2d].s_box = ",r);
	//printState(state);

	shift_rows(state, ENC);

	//printf("R[%2d].s_row = ",r);
	//printState(state);

	select_round_key(r,roundkey);

	//printf("R[%2d].s_sch = ",r);
	//printKey(roundkey);

	add_round_key(state,  roundkey);
	//printf("\toutput: ");
	//printState(state);

}

int isHex(char c)
{
	if(isdigit(c) || (isalpha(c) && toupper(c) <= 'F'))
		return 1;
	return 0;
}

int setup(int key_size, unsigned char* skey)
{
	int i,val;
	char tmp[3] = "00";
	unsigned char key[NK][4];
	/*
		s'assurer du mode aes choisi (128,192,256)
	*/
	switch(key_size)
	{
		case AES_128:
			NR = 10;
			NK = 4;
			break;

		case AES_192:
			NR = 12;
			NK = 6;
			break;
		case AES_256:
			NR = 14;
			NK = 8;
			break;
		default:
			return FAIL;
	}
	/*
		vérifier la taille de la clé(en hex)
		ex: pour une clé de 128bits NK=4 len(key) = 32 = 4<<3
	*/
	if((int)strlen((const char*)skey) != NK<<3)
	{
		printf("invalid key length: %s\n",skey);
		return FAIL;
	}

	/*
		conversion de la clé en matrice de NK*4
	*/
	for(i=0; i<(int)strlen((const char*)skey); i+=2)
	{
		if(!isHex(skey[i]) || !isHex(skey[i+1]))
		{
			printf("invalid key: %c%c is not hex\n",skey[i],skey[i+1]);
			return FAIL;
		}
		strncpy(tmp, (char *)skey+i, 2);
		sscanf(tmp, "%x", &val);

		key[(i/2)/4][(i/2)%4] = val;
	}

	/*
		on effecture l'opération d'éxpension de la clé
		en NR+1 clés de taille 128 bits
	*/
	key_expension(key);

	return SUCCESS;
}


