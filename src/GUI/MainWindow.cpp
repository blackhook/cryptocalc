#include "MainWindow.h"
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPlainTextEdit>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QFileDialog>
#include <QRadioButton>
#include <QCheckBox>
#include <QDir>
#include <QProgressBar>
#include <QByteArray>
#include <QSharedPointer>
#include <QMessageBox>
#include <QDockWidget>
#include "Core/GuiBridge.h"


/***************************************************************************//*!
* @brief Constructeur
* @param[in,opt] parent		Le parent de la window. (default: 0)
* @return[NONE]
*/
MainWindow::MainWindow( QWidget* parent ) : QMainWindow(parent)
{
	this->setWindowTitle(tr("Crypto Calculator"));
	this->setStyleSheet("#border_1,#border_2,#border_3 {border: 1px solid #000; border-radius:8px;}");

	QWidget* centralWidget = new QWidget(this);

	this->setCentralWidget(centralWidget);

	QGridLayout* layout = new QGridLayout(centralWidget);
	centralWidget->setLayout(layout);

	QHBoxLayout* hb = 0;
	QFrame* line = 0;
#	define QFRAME_HR_LINE	line = new QFrame(); \
							line->setFrameShape(QFrame::HLine); \
							line->setFrameShadow(QFrame::Sunken); \
							vb->addWidget(line)

	{// Data
		QFrame* border = new QFrame(centralWidget);
		border->setObjectName("border_1");
		QVBoxLayout* vb = new QVBoxLayout(border);
		border->setLayout(vb);
		layout->addWidget(border, 0, 0, 1, 1);
		vb->addWidget(new QLabel(tr("<b>Données a utiliser</b>"), border));
		vb->addWidget(m_data_direct_input = new QRadioButton(tr("Input direct"), border));
		connect(m_data_direct_input, SIGNAL(clicked()), this, SLOT(radioSelector()));
		vb->addWidget(m_data = new QPlainTextEdit(border));
		// Checkbox - Base64
		{
			QFrame* border2 = new QFrame(border);
			vb->addWidget(border2);
			QHBoxLayout* hb = new QHBoxLayout(border2);
			hb->addWidget(m_dataConverter[CHKBOX_BASE64] = new QRadioButton(tr("Base64"), border2));
			hb->addWidget(m_dataConverter[CHKBOX_HEXA] = new QRadioButton(tr("Hexa"), border2));
			hb->addWidget(m_dataConverter[CHKBOX_NONE] = new QRadioButton(tr("Normal"), border2));
			m_dataConverter[CHKBOX_NONE]->setChecked(true);
		}
		QFRAME_HR_LINE;
		vb->addWidget(m_data_file_input = new QRadioButton(tr("Input avec un fichier"), border));
		connect(m_data_file_input, SIGNAL(clicked()), this, SLOT(radioSelector()));
		vb->addLayout(hb = new QHBoxLayout());
		hb->addWidget(m_data_label_file_input = new QLabel(border));
		hb->addWidget(m_data_file_search = new QPushButton(tr("Parcourrir"), border));
		connect(m_data_file_search, SIGNAL(clicked()), this, SLOT(data_file_selector()));
		m_data_direct_input->setChecked(true);
		radioSelector(m_data_direct_input);
	}
	{// Clé
		QFrame* border = new QFrame(centralWidget);
		border->setObjectName("border_2");
		QVBoxLayout* vb = new QVBoxLayout(border);
		layout->addWidget(border, 0, 1, 1, 1);

		vb->addWidget(new QLabel(tr("<b>Clé a utiliser</b>"), border));
		vb->addWidget(m_key_direct_input = new QRadioButton(tr("Input direct"), border));
		connect(m_key_direct_input, SIGNAL(clicked()), this, SLOT(radioSelector()));
		vb->addWidget(m_key = new QPlainTextEdit(border));
		// Checkbox - Base64
		{
			QFrame* border2 = new QFrame(border);
			vb->addWidget(border2);
			QHBoxLayout* hb = new QHBoxLayout(border2);
			hb->addWidget(m_keyConverter[CHKBOX_BASE64] = new QRadioButton(tr("Base64"), border2));
			hb->addWidget(m_keyConverter[CHKBOX_HEXA] = new QRadioButton(tr("Hexa"), border2));
			hb->addWidget(m_keyConverter[CHKBOX_NONE] = new QRadioButton(tr("Normal"), border2));
			m_keyConverter[CHKBOX_NONE]->setChecked(true);
		}
		QFRAME_HR_LINE;
		vb->addWidget(m_key_file_input = new QRadioButton(tr("Input avec un fichier"), border));
		connect(m_key_file_input, SIGNAL(clicked()), this, SLOT(radioSelector()));
		vb->addLayout(hb = new QHBoxLayout());
		hb->addWidget(m_key_label_file_input = new QLabel(border));
		hb->addWidget(m_key_file_search = new QPushButton(tr("Parcourrir"), border));
		connect(m_key_file_search, SIGNAL(clicked()), this, SLOT(key_file_selector()));
		m_key_direct_input->setChecked(true);
		radioSelector(m_key_direct_input);
	}
	//{// Selection de la fonction de crypto
		m_cryptoSelector = new QComboBox(centralWidget);
		layout->addWidget(m_cryptoSelector,								1, 0, 1, 2);
		GuiBridge::makeGUI(m_cryptoSelector);
		connect(m_cryptoSelector, SIGNAL(currentIndexChanged(QString)), this, SLOT(cipherChanged(QString)));
	//}
	{// Sortie
		QFrame* border = new QFrame(centralWidget);
		border->setObjectName("border_3");
		QVBoxLayout* vb = new QVBoxLayout(border);
		layout->addWidget(border, 2, 0, 1, 2);

		vb->addWidget(new QLabel(tr("<b>Sortie du résultat</b>"), border));
		vb->addWidget(m_output_direct_input = new QRadioButton(tr("Sortie direct"), border));
		connect(m_output_direct_input, SIGNAL(clicked()), this, SLOT(radioSelector()));
		vb->addWidget(m_output = new QPlainTextEdit(border));
		// Checkbox - Base64
		{
			QFrame* border2 = new QFrame(border);
			vb->addWidget(border2);
			QHBoxLayout* hb = new QHBoxLayout(border2);
			hb->addWidget(m_outputConverter[CHKBOX_BASE64] = new QRadioButton(tr("Base64"), border2));
			hb->addWidget(m_outputConverter[CHKBOX_HEXA] = new QRadioButton(tr("Hexa"), border2));
			hb->addWidget(m_outputConverter[CHKBOX_NONE] = new QRadioButton(tr("Normal"), border2));
			m_outputConverter[CHKBOX_BASE64]->setChecked(true);
		}
		QFRAME_HR_LINE;
		vb->addWidget(m_output_file_input = new QRadioButton(tr("Sortie dans un fichier"), border));
		connect(m_output_file_input, SIGNAL(clicked()), this, SLOT(radioSelector()));
		vb->addLayout(hb = new QHBoxLayout());
		hb->addWidget(m_output_label_file_input = new QLabel(border));
		hb->addWidget(m_output_file_search = new QPushButton(tr("Parcourrir"), border));
		connect(m_output_file_search, SIGNAL(clicked()), this, SLOT(output_file_selector()));
		m_output_direct_input->setChecked(true);
		radioSelector(m_output_direct_input);
	}

	// Bouton de cryptage
	QPushButton* tmp=0;
	layout->addWidget(tmp = new QPushButton(tr("Crypter"), centralWidget),			3, 0);
	connect(tmp, SIGNAL(clicked()), this, SLOT(cipher()));
	layout->addWidget(m_unCipher = new QPushButton(tr("Décrypter"), centralWidget),	3, 1);
	connect(m_unCipher, SIGNAL(clicked()), this, SLOT(uncipher()));

	{// Log
		layout->addWidget(m_showLog = new QCheckBox(tr("Activer le log"), centralWidget), 4, 0, 1, 2);
		connect(m_showLog, SIGNAL(clicked()), this, SLOT(toogleLogActivity()));
		m_logWindow = new QDockWidget(tr("Log"), this);
		m_logWindow->setAllowedAreas(Qt::AllDockWidgetAreas);
		m_logWindow->setWidget(m_log = new QPlainTextEdit(m_logWindow));
		m_log->setReadOnly(true);
		m_log->setTabStopWidth(20);
		this->addDockWidget(Qt::RightDockWidgetArea, m_logWindow);
		m_logWindow->setVisible(false);
		m_logWindow->setMinimumWidth(400);
	}

	{// Key n°2
		m_key2Window = new QDockWidget(tr("Clé n°2"), this);
		m_key2Window->setAllowedAreas(Qt::AllDockWidgetAreas);
		QFrame* border = new QFrame(m_key2Window);
		m_key2Window->setWidget(border);
		this->addDockWidget(Qt::RightDockWidgetArea, m_key2Window);
		m_key2Window->setVisible(false);
		m_key2Window->setMinimumWidth(200);

		QVBoxLayout* vb = new QVBoxLayout(border);

		vb->addWidget(new QLabel(tr("<b>Clé a utiliser</b>"), border));
		vb->addWidget(m_key2_direct_input = new QRadioButton(tr("Input direct"), border));
		connect(m_key2_direct_input, SIGNAL(clicked()), this, SLOT(radioSelector()));
		vb->addWidget(m_key2 = new QPlainTextEdit(border));
		// Checkbox - Base64
		{
			QFrame* border2 = new QFrame(border);
			vb->addWidget(border2);
			QHBoxLayout* hb = new QHBoxLayout(border2);
			hb->addWidget(m_key2Converter[CHKBOX_BASE64] = new QRadioButton(tr("Base64"), border2));
			hb->addWidget(m_key2Converter[CHKBOX_HEXA] = new QRadioButton(tr("Hexa"), border2));
			hb->addWidget(m_key2Converter[CHKBOX_NONE] = new QRadioButton(tr("Normal"), border2));
			m_key2Converter[CHKBOX_NONE]->setChecked(true);
		}
		QFRAME_HR_LINE;
		vb->addWidget(m_key2_file_input = new QRadioButton(tr("Input avec un fichier"), border));
		connect(m_key2_file_input, SIGNAL(clicked()), this, SLOT(radioSelector()));
		vb->addLayout(hb = new QHBoxLayout());
		hb->addWidget(m_key2_label_file_input = new QLabel(border));
		hb->addWidget(m_key2_file_search = new QPushButton(tr("Parcourrir"), border));
		connect(m_key2_file_search, SIGNAL(clicked()), this, SLOT(key_file_selector()));
		m_key2_direct_input->setChecked(true);
		radioSelector(m_key2_direct_input);
	}

	// Barre de progression
	layout->addWidget(m_loading = new QProgressBar(centralWidget),			5, 0, 1, 2);
	m_loading->setValue(0);

	// Rémanence de dernier algo de crypto
	QFile fp("./last_crypto.txt");
	if( fp.open(QIODevice::ReadOnly) )
		m_cryptoSelector->setCurrentIndex(m_cryptoSelector->findText(QString(fp.readAll())));
	cipherChanged(m_cryptoSelector->currentText());
}


/***************************************************************************//*!
* @brief [SLOT] On cherche à crypter les données.
* @return[NONE]
*/
void MainWindow::cipher()
{
	m_log->setPlainText("");
	m_loading->setValue(0);
	QByteArray data;
	bool isDataFile=false;
	if( m_data_direct_input->isChecked() ){
		data = m_data->toPlainText().toUtf8();
		if( m_dataConverter[CHKBOX_HEXA]->isChecked() ){
			data = QByteArray::fromHex(data);
		}else if( m_dataConverter[CHKBOX_BASE64]->isChecked() ){
			data = QByteArray::fromBase64(data);
		}
	}else{
		data = m_data_label_file_input->text().toUtf8();
		isDataFile = true;
	}

	QByteArray key;
	bool isKeyFile=false;
	if( m_key_direct_input->isChecked() ){
		key = m_key->toPlainText().toUtf8();
		if( m_keyConverter[CHKBOX_HEXA]->isChecked() ){
			key = QByteArray::fromHex(data);
		}else if( m_keyConverter[CHKBOX_BASE64]->isChecked() ){
			key = QByteArray::fromBase64(data);
		}
	}else{
		key = m_key_label_file_input->text().toUtf8();
		isKeyFile = true;
	}

	QByteArray key2;
	bool isKey2File=false;
	if( m_key2_direct_input->isChecked() ){
		key2 = m_key2->toPlainText().toUtf8();
		if( m_key2Converter[CHKBOX_HEXA]->isChecked() ){
			key2 = QByteArray::fromHex(data);
		}else if( m_key2Converter[CHKBOX_BASE64]->isChecked() ){
			key2 = QByteArray::fromBase64(data);
		}
	}else{
		key2 = m_key2_label_file_input->text().toUtf8();
		isKey2File = true;
	}

	QByteArray output;
	if( !m_output_direct_input->isChecked() )
		output = m_output_label_file_input->text().toUtf8();

	GuiBridge* runner = 0;

	runner = new GuiBridge(
		this,
		m_cryptoSelector->currentText(),
		true,
		data,
		isDataFile,
		key,
		isKeyFile,
		key2,
		isKey2File,
		output
	);


	qRegisterMetaType< QSharedPointer<QByteArray> >("QSharedPointer<QByteArray>");
	connect(runner, SIGNAL(cipherFuncFinish(QSharedPointer<QByteArray>)), this, SLOT(cihperFuncFinish(QSharedPointer<QByteArray>)));
	connect(runner, SIGNAL(progress(unsigned char)), this, SLOT(progress(unsigned char)));
	connect(runner, SIGNAL(error(QString)), this, SLOT(onError(QString)));
	connect(runner, SIGNAL(log(QString)), this, SLOT(onLog(QString)));

	this->setEnabled(false);
	runner->start();
}


/***************************************************************************//*!
* @brief [SLOT] On cherche à décrypter les données.
* @return[NONE]
*/
void MainWindow::uncipher()
{
	m_log->setPlainText("");
	m_loading->setValue(0);
	QByteArray data;
	bool isDataFile=false;
	if( m_data_direct_input->isChecked() ){
		data = m_data->toPlainText().toUtf8();
		if( m_dataConverter[CHKBOX_HEXA]->isChecked() ){
			data = QByteArray::fromHex(data);
		}else if( m_dataConverter[CHKBOX_BASE64]->isChecked() ){
			data = QByteArray::fromBase64(data);
		}
	}else{
		data = m_data_label_file_input->text().toUtf8();
		isDataFile = true;
	}

	QByteArray key;
	bool isKeyFile=false;
	if( m_key_direct_input->isChecked() ){
		key = m_key->toPlainText().toUtf8();
		if( m_keyConverter[CHKBOX_HEXA]->isChecked() ){
			key = QByteArray::fromHex(data);
		}else if( m_keyConverter[CHKBOX_BASE64]->isChecked() ){
			key = QByteArray::fromBase64(data);
		}
	}else{
		key = m_key_label_file_input->text().toUtf8();
		isKeyFile = true;
	}

	QByteArray key2;
	bool isKey2File=false;
	if( m_key2_direct_input->isChecked() ){
		key2 = m_key2->toPlainText().toUtf8();
		if( m_key2Converter[CHKBOX_HEXA]->isChecked() ){
			key2 = QByteArray::fromHex(data);
		}else if( m_key2Converter[CHKBOX_BASE64]->isChecked() ){
			key2 = QByteArray::fromBase64(data);
		}
	}else{
		key2 = m_key2_label_file_input->text().toUtf8();
		isKey2File = true;
	}

	QByteArray output;
	if( !m_output_direct_input->isChecked() )
		output = m_output_label_file_input->text().toUtf8();

	GuiBridge* runner=0;

	runner = new GuiBridge(
		this,
		m_cryptoSelector->currentText(),
		false,
		data,
		isDataFile,
		key,
		isKeyFile,
		key2,
		isKey2File,
		output
	);

	qRegisterMetaType< QSharedPointer<QByteArray> >("QSharedPointer<QByteArray>");
	connect(runner, SIGNAL(cipherFuncFinish(QSharedPointer<QByteArray>)), this, SLOT(cihperFuncFinish(QSharedPointer<QByteArray>)));
	connect(runner, SIGNAL(progress(unsigned char)), this, SLOT(progress(unsigned char)));
	connect(runner, SIGNAL(error(QString)), this, SLOT(onError(QString)));
	connect(runner, SIGNAL(log(QString)), this, SLOT(onLog(QString)));

	this->setEnabled(false);
	runner->start();
}


/***************************************************************************//*!
* @brief [SLOT] Action lors du changement d'algo
* @param[in] newCipher		Le nouvelle algo de crypto a utiliser.
* @return[NONE]
*/
void MainWindow::cipherChanged( QString newCipher )
{
	if( newCipher.isEmpty() )
		return ;

	// Rémanence de dernier algo de crypto
	QFile fp("./last_crypto.txt");
	if( fp.open(QIODevice::WriteOnly | QIODevice::Truncate) )
		fp.write(newCipher.toLatin1().data());

	char flags = GuiBridge::cipherInfo(newCipher)->flags;

	m_key_direct_input->setEnabled(flags & GuiBridge::CF_1KeyNeeded);
	m_key_file_input->setEnabled(flags & GuiBridge::CF_1KeyNeeded);
	m_key_label_file_input->setEnabled(flags & GuiBridge::CF_1KeyNeeded);
	m_key_file_search->setEnabled(flags & GuiBridge::CF_1KeyNeeded);
	m_key->setEnabled(flags & GuiBridge::CF_1KeyNeeded);
	for( unsigned char i=0; i<3; ++i )
		m_keyConverter[i]->setEnabled(flags & GuiBridge::CF_1KeyNeeded);

	m_key2Window->setVisible(flags & GuiBridge::CF_2KeyNeeded);
	m_unCipher->setEnabled(flags & GuiBridge::CF_canUnCipher);
}


/***************************************************************************//*!
* @brief [SLOT] Demande de selection du fichier a crypter.
* @return[NONE]
*/
void MainWindow::data_file_selector()
{
	QString ret = QFileDialog::getOpenFileName(this, tr("Le fichier a crypter"), QDir::homePath());
	if( ret.isNull() || ret.isEmpty() )
		return ;
	m_data_label_file_input->setText(ret);
}


/***************************************************************************//*!
* @brief [SLOT] Demande de selection de la clé a utiliser pour le cryptage.
* @return[NONE]
*
* @warning Utilise sender()
*/
void MainWindow::key_file_selector()
{
	QString ret = QFileDialog::getOpenFileName(this, tr("La clé a utiliser"), QDir::homePath());
	if( ret.isNull() || ret.isEmpty() )
		return ;

	if( sender() == m_key_file_search ){
		m_key_label_file_input->setText(ret);
	}else{// sender() == m_key2_file_search
		m_key2_label_file_input->setText(ret);
	}
}


/***************************************************************************//*!
* @brief [SLOT] Demande de selection du fichier de sortie.
* @return[NONE]
*/
void MainWindow::output_file_selector()
{
	QString ret = QFileDialog::getSaveFileName(this, tr("Enregistrer le résultat dans ..."), QDir::homePath());
	if( ret.isNull() || ret.isEmpty() )
		return ;
	m_output_label_file_input->setText(ret);
}


/***************************************************************************//*!
* @brief [SLOT] Permet de déterminer ce qui doit être afficher en fonction de ce
* qui est sélectionné ({from / sender()})
* @param[in,opt] from	Quel radio bouton est selectionné ? (Peut être NULL si sender() non NULL)
* @return[NONE]
*
* @warning Utilise sender() si {from} = 0
*/
void MainWindow::radioSelector( QRadioButton* from )
{
	if( from == 0 )
		from = static_cast<QRadioButton*>(sender());

	if( from == m_data_direct_input ){
		m_data_file_search->setEnabled(false);
		m_data_label_file_input->setEnabled(false);
		m_data->setEnabled(true);
		for( unsigned char i=0; i<3; ++i )
			m_dataConverter[i]->setEnabled(true);
		return ;
	}
	if( from == m_data_file_input ){
		m_data_file_search->setEnabled(true);
		m_data_label_file_input->setEnabled(true);
		m_data->setEnabled(false);
		for( unsigned char i=0; i<3; ++i )
			m_dataConverter[i]->setEnabled(false);
		return ;
	}
	if( from == m_key_direct_input ){
		m_key_file_search->setEnabled(false);
		m_key_label_file_input->setEnabled(false);
		m_key->setEnabled(true);
		for( unsigned char i=0; i<3; ++i )
			m_keyConverter[i]->setEnabled(true);
		return ;
	}
	if( from == m_key_file_input ){
		m_key_file_search->setEnabled(true);
		m_key_label_file_input->setEnabled(true);
		m_key->setEnabled(false);
		for( unsigned char i=0; i<3; ++i )
			m_keyConverter[i]->setEnabled(false);
		return ;
	}
	if( from == m_key2_direct_input ){
		m_key2_file_search->setEnabled(false);
		m_key2_label_file_input->setEnabled(false);
		m_key2->setEnabled(true);
		for( unsigned char i=0; i<3; ++i )
			m_key2Converter[i]->setEnabled(true);
		return ;
	}
	if( from == m_key2_file_input ){
		m_key2_file_search->setEnabled(true);
		m_key2_label_file_input->setEnabled(true);
		m_key2->setEnabled(false);
		for( unsigned char i=0; i<3; ++i )
			m_key2Converter[i]->setEnabled(false);
		return ;
	}
	if( from == m_output_direct_input ){
		m_output_file_search->setEnabled(false);
		m_output_label_file_input->setEnabled(false);
		m_output->setEnabled(true);
		for( unsigned char i=0; i<3; ++i )
			m_outputConverter[i]->setEnabled(true);
		return ;
	}
	if( from == m_output_file_input ){
		m_output_file_search->setEnabled(true);
		m_output_label_file_input->setEnabled(true);
		m_output->setEnabled(false);
		for( unsigned char i=0; i<3; ++i )
			m_outputConverter[i]->setEnabled(false);
		return ;
	}
}


/***************************************************************************//*!
* @brief [SLOT] Permet de détecter la fin d'un cryptage/décryptage
* @param[in] data		Les données a afficher dans output.
* @return[NONE]
*/
void MainWindow::cihperFuncFinish( QSharedPointer<QByteArray> data )
{
	this->setEnabled(true);
	if( m_outputConverter[CHKBOX_HEXA]->isChecked() ){
		m_output->setPlainText(QString(data.data()->toHex()));
	}else if( m_outputConverter[CHKBOX_BASE64]->isChecked() ){
		m_output->setPlainText(data.data()->toBase64());
	}else{
		m_output->setPlainText(*(data.data()));
	}
}


/***************************************************************************//*!
* @brief [SLOT] Permet de mesurer l'avancement d'un cryptage/décryptage
* @param[in] percent		Pourcentage d'avancement
* @return[NONE]
*/
void MainWindow::progress( u_int8_t percent )
{
	m_loading->setValue(percent);
}


/***************************************************************************//*!
* @brief [SLOT] Action lors d'une erreur lors d'un cryptage/décryptage.
* Va afficher le message d'erreur dans une Box.
* @param[in] msg		Message d'erreur
* @return[NONE]
*/
void MainWindow::onError( QString msg )
{
	QMessageBox::critical(this, QObject::tr("Erreur"), msg);
	this->setEnabled(true);
}


/***************************************************************************//*!
* @brief [SLOT] Action lors d'une demande de log d'erreur.
* Va afficher le message d'erreur dans une Box.
* @param[in] msg		Message a logguer
* @return[NONE]
*/
void MainWindow::onLog( QString msg )
{
	if( !m_showLog->isChecked() )
		return ;
	m_log->setPlainText(m_log->toPlainText()+msg);
}


/***************************************************************************//*!
* @brief [SLOT] Action lors d'une demande de log d'erreur.
* Va afficher le message d'erreur dans une Box.
* @param[in] msg		Message a logguer
* @return[NONE]
*/
void MainWindow::toogleLogActivity()
{
	m_logWindow->setVisible(m_showLog->isChecked());
}
