#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#ifdef OS_LINUX
#	include <stdint-gcc.h>
#else
#	include <stdint.h>
#endif


class QPlainTextEdit;
class QLabel;
class QRadioButton;
class QPushButton;
class QProgressBar;
class QComboBox;
template <class T> class QSharedPointer;
class QByteArray;
class QCheckBox;
class QDockWidget;


/***************************************************************************//*!
* @brief Cette classe créée et gère la fenêtre principale.
*/
class MainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		/*!
		 * @brief Permet de manipuler facilement les m_{data|key|key2|output}Converter[X]
		 */
		enum {
			CHKBOX_BASE64 = 0,
			CHKBOX_HEXA = 1,
			CHKBOX_NONE = 2
		};


	protected:
		QPlainTextEdit*			m_data;//!< Les données a crypter
		QPlainTextEdit*			m_key;//!< La clé a utiliser
		QPlainTextEdit*			m_output;//!< Résultat
		QLabel*					m_data_label_file_input;//!< Fichier a crypter
		QLabel*					m_key_label_file_input;//!< Fichier contenant la clé de cryptage.
		QLabel*					m_output_label_file_input;//!< Fichier de résultat.
		QRadioButton*			m_data_direct_input;
		QRadioButton*			m_data_file_input;
		QRadioButton*			m_key_direct_input;
		QRadioButton*			m_key_file_input;
		QRadioButton*			m_output_direct_input;
		QRadioButton*			m_output_file_input;
		QPushButton*			m_data_file_search;
		QPushButton*			m_key_file_search;
		QPushButton*			m_output_file_search;
		QProgressBar*			m_loading;//!< Chargement
		QPushButton*			m_unCipher;//!< Le bouton de déchiffrement
		QComboBox*				m_cryptoSelector;//!< Fonction de crypto dispo
		QRadioButton*			m_dataConverter[3];//!< Parser l'entrée en base64, hexa, normal
		QRadioButton*			m_keyConverter[3];//!< Parser la clé en base64, hexa, normal
		QRadioButton*			m_key2Converter[3];//!< Parser la clé en base64, hexa, normal
		QRadioButton*			m_outputConverter[3];//!< Parser la sortie en base64, hexa, normal

		QCheckBox*				m_showLog;//!< Afficher le log ?
		QDockWidget*			m_logWindow;//!< Fênetre de log
		QPlainTextEdit*			m_log;//!< Résultat

		QDockWidget*			m_key2Window;//!< Fênetre de log
		QPlainTextEdit*			m_key2;//!< La clé n°2 (pour le 3DES)
		QRadioButton*			m_key2_direct_input;//!< Entrée direct de la clé n°2
		QRadioButton*			m_key2_file_input;//!< Entrée de la clé n°2 avec un fichier
		QPushButton*			m_key2_file_search;//!< Bouton de recherche de la clé n°2
		QLabel*					m_key2_label_file_input;//!< Fichier contenant la clé de cryptage.
		QCheckBox*				m_key2_from_base64;//!< Parser la sortie en base64 ?

	public:
		explicit MainWindow( QWidget* parent=0 );


	protected slots:
		void cipher();
		void uncipher();
		void cipherChanged( QString newCipher );
		void data_file_selector();
		void key_file_selector();
		void output_file_selector();
		void radioSelector( QRadioButton* sender=0 );
		void cihperFuncFinish( QSharedPointer<QByteArray> data );
		void progress( unsigned char percent );
		void onError( QString msg );
		void onLog( QString msg );
		void toogleLogActivity();
};

#endif // MAINWINDOW_H
