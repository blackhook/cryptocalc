#!/bin/sh
cd bin/
echo -n 'Xor-CBC\n\t'
echo -n 'ABCDEFGH' | ./CryptoCalc enc -xor-cbc -k ABCDEFGH | ./CryptoCalc dec -xor-cbc -k ABCDEFGH
echo
echo -n 'DES\n\t'
echo -n 'ABCDEFGH' | ./CryptoCalc enc -des -k ABCDEFGH | base64
echo -n 'DES -> OpenSSL\n\t'
echo -n 'ABCDEFGH' | openssl enc -des-cbc -nosalt -k ABCDEFGH | base64
echo -n '3DES -> OpenSSL\n\t'
echo -n 'ABCDEFGH' | openssl enc -des3 -nosalt -k ABCDEFGH | base64
