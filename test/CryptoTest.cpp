#include <QtTest/QtTest>
#include <QDebug>
#include "CryptoTest.h"
#include "Core/GuiBridge.h"
#include "Crypto/xor/xor.h"
#include "Crypto/des/des.h"
#include "Crypto/aes/aes.h"
#include "Crypto/blowfish/blowfish.h"
#include "GUI/MainWindowTest.h"

#define CryptoSym( algo, info, msg, key, result ) QTest::newRow(#algo) << (void*) algo << info << QByteArray(msg) << QByteArray(key) << false << QByteArray("") << QByteArray(result)
#define CryptoSym2( algo, info, msg, key, key2, result ) QTest::newRow(#algo) << (void*) algo << info << QByteArray(msg) << QByteArray(key) << true << QByteArray(key2) << QByteArray(result)




/***************************************************************************//*!
* @brief Tests sur les fonctions de crypto symétriques.
* @return[NONE]
*
* @note NE pas toucher cette fonction pour ajouter des données a tester, voir
* CryptoTest::testCryptoSym_data
*/
void CryptoTest::testCryptoSym()
{
	QFETCH(void*,		func);
	QFETCH(QString,		info);
	QFETCH(QByteArray,	msg);
	QFETCH(QByteArray,	key);
	QFETCH(bool,		has2keys);
	QFETCH(QByteArray,	key2);
	QFETCH(QByteArray,	result);
	bool ret = false;

	GuiBridge::CipherFunction_1Key ptr_func = (GuiBridge::CipherFunction_1Key) func;
	GuiBridge::CipherFunction_2Keys ptr_func2 = (GuiBridge::CipherFunction_2Keys) func;

	if( info.size() ){
		qDebug() << QString("Encrypt=true, Info="+info).toUtf8().constData();
	}else{
		qDebug() << "Encrypt=true";
	}
	// bool xor_cbc( CryptoStream::CryptoStream_t* stream, const char pass[], size_t passSize, bool isEncryptMod );
	try{
		if( has2keys ){
			ret = ptr_func2(initStream(msg), key.data(), key.size(), key2.data(), key2.size(), true);
		}else{
			ret = ptr_func(initStream(msg), key.data(), key.size(), true);
		}
	}catch( const char* msg ){
		//qDebug() << "[TRHOW] " << msg;
		QFAIL((QString("[TRHOW] ")+QString(msg)).toUtf8().constData());
	}

	QCOMPARE(ret, true);
	QCOMPARE(QString(m_stream.writer.m_str->toBase64()), QString(result));

	qDebug() << "Encrypt=false";
	try{
		if( has2keys ){
			ret = ptr_func2(initStream(QByteArray::fromBase64(result)), key.data(), key.size(), key2.data(), key2.size(), false);
		}else{
			ret = ptr_func(initStream(QByteArray::fromBase64(result)), key.data(), key.size(), false);
		}
	}catch( const char* msg ){
		//qDebug() << "[TRHOW] " << msg;
		QFAIL((QString("[TRHOW] ")+QString(msg)).toUtf8().constData());
	}

	QCOMPARE(ret, true);
	if( m_stream.writer.m_str->size() != msg.size() )
		qDebug() << "[Error] Not same size:\n"
					"    Actual:   " << m_stream.writer.m_str->size() << "\n"
					"    Expected: " << msg.size() << "\n"
					"    Actual value (b64):   " << m_stream.writer.m_str->toBase64() << "\n"
					"    Expected value (b64): " << msg.toBase64() << "\n"
					"    Actual value:   " << QString(*m_stream.writer.m_str) << "\n"
					"    Expected value: " << QString(msg) << "\n";
	QCOMPARE(QString(*(m_stream.writer.m_str)), QString(msg));
}


/***************************************************************************//*!
* @brief Données de tests sur les fonctions de crypto symétriques.
* @return[NONE]
*/
void CryptoTest::testCryptoSym_data()
{
	QTest::addColumn<void*>("func");
	QTest::addColumn<QString>("info");
	QTest::addColumn<QByteArray>("msg");
	QTest::addColumn<QByteArray>("key");
	QTest::addColumn<bool>("has2keys");
	QTest::addColumn<QByteArray>("key2");
	QTest::addColumn<QByteArray>("result");

	// CryptoSym(<FONCTION DE CRYPTO>, "Msg", "Key", "Résultat attendu en base64");
	CryptoSym(Crypt::xor_cbc,		"", "ABCDEFGH", "ABCDEFGH", "KioqKioqKio=");
	CryptoSym(Crypt::des,			"", "ABCDEFGH", "ABCDEFGH", "zPrhPp+NGFo=");
	CryptoSym2(Crypt::blowfish_ECB,	"", "azert", "azertyui", "12345678", "Vr9hwIaVC/s=");
	CryptoSym2(Crypt::blowfish_CBC,	"", "azert", "azertyui", "12345678", "MTIzNDU2NzhuVvZKRB67wg==");

	//CryptoSym(Crypt::aes_128_CBC,	"Test du padding", "ABCDEFGH", "000102030405060708090a0B0C0D0E0F", "oMARRCvwNArq1nSe3ou3Ng==");
	//CryptoSym(Crypt::aes_128_CBC,	"", "ABCDEFGHABCDEFGHABCDEFGHABCDEFGH", "000102030405060708090a0B0C0D0E0F", "hElkzHIpIBtx/7bAR5uKNWZBzaKfdBgrjAespr2myQY=");
	//CryptoSym(Crypt::aes_128_ECB,	"", "ABCDEFGHABCDEFGHABCDEFGHABCDEFGH", "000102030405060708090a0B0C0D0E0F", "hElkzHIpIBtx/7bAR5uKNYRJZMxyKSAbcf+2wEebijU=");

	//CryptoSym(Crypt::aes_192_CBC,	"Test segfault", "ABCDEFGH", "000102030405060708090a0B0C0D0E0FASKYTNGPLWFPOFGi", "hElkzHIpIBtx/7bAR5uKNYRJZMxyKSAbcf+2wEebijU=");
	//CryptoSym(Crypt::aes_192_CBC,	"Test du padding", "ABCDEFGH", "000102030405060708090a0B0C0D0E0FB00B00B00BB00B00", "hElkzHIpIBtx/7bAR5uKNYRJZMxyKSAbcf+2wEebijU=");
	//CryptoSym(Crypt::aes_192_CBC,	"", "ABCDEFGHABCDEFGHABCDEFGHABCDEFGH", "000102030405060708090a0B0C0D0E0FASKYTNGPLWFPOFGi", "hElkzHIpIBtx/7bAR5uKNYRJZMxyKSAbcf+2wEebijU=");
	//CryptoSym(Crypt::aes_192_ECB,	"", "ABCDEFGHABCDEFGHABCDEFGHABCDEFGH", "000102030405060708090a0B0C0D0E0FASKYTNGPLWFPOFGi", "hElkzHIpIBtx/7bAR5uKNYRJZMxyKSAbcf+2wEebijU=");
}


/***************************************************************************//*!
* @brief Permet d'init le {stream}.
* @param[in] input		Les données a crypter / décrypter.
* @return Le {stream}. (Identique à m_stream).
*/
CryptoStream::CryptoStream_t* CryptoTest::initStream( const QByteArray& input )
{
	if( m_stream.reader.m_str )
		delete m_stream.reader.m_str;
	m_stream.reader.m_str = new QByteArray(input);
	m_stream.m_readerSize = input.size();
	m_stream.m_fpos = 0;
	if( m_stream.writer.m_str )
		delete m_stream.writer.m_str;
	m_stream.writer.m_str = new QByteArray();
	return &m_stream;
}


/***************************************************************************//*!
* @brief Fonction appelée une fois tous les tests effectués.
* @return[NONE]
*/
void CryptoTest::cleanupTestCase()
{
	if( m_stream.reader.m_str ){
		delete m_stream.reader.m_str;
		m_stream.reader.m_str = 0;
	}
	m_stream.m_readerSize = 0;
	m_stream.m_fpos = 0;
	if( m_stream.writer.m_str ){
		delete m_stream.writer.m_str;
		m_stream.writer.m_str = 0;
	}
}


/***************************************************************************//*!
* @brief Tests sur l'interface graphique.
* @return[NONE]
*/
void CryptoTest::testGUI()
{
	GuiBridge::registerGUI("XOR-CBC", Crypt::xor_cbc);
	MainWindowTest mw;

	{
		qDebug() << "Encrypt";
		mw.data()->setPlainText("ABCDEFGH");
		mw.key()->setPlainText("ABCDEFGH");
		mw.output()->setPlainText("CLEAN THIS");
		mw.data_direct_input()->setChecked(true);
		mw.key_direct_input()->setChecked(true);
		mw.output_direct_input()->setChecked(true);
		// data = none
		mw.dataConverter()[MainWindow::CHKBOX_BASE64]->setChecked(false);
		mw.dataConverter()[MainWindow::CHKBOX_HEXA]->setChecked(false);
		mw.dataConverter()[MainWindow::CHKBOX_NONE]->setChecked(true);
		// key = none
		mw.keyConverter()[MainWindow::CHKBOX_BASE64]->setChecked(false);
		mw.keyConverter()[MainWindow::CHKBOX_HEXA]->setChecked(false);
		mw.keyConverter()[MainWindow::CHKBOX_NONE]->setChecked(true);
		// output = base64
		mw.outputConverter()[MainWindow::CHKBOX_HEXA]->setChecked(false);
		mw.outputConverter()[MainWindow::CHKBOX_NONE]->setChecked(false);
		mw.outputConverter()[MainWindow::CHKBOX_BASE64]->setChecked(true);
		QVERIFY2(mw.cryptoSelector()->findText("XOR-CBC") != -1, "Unable to find XOR-CBC !");
		mw.cryptoSelector()->setCurrentIndex(mw.cryptoSelector()->findText("XOR-CBC"));
		mw._cipher();
		for( int i=0; i<15 && mw.output()->toPlainText() == "CLEAN THIS"; ++i )
			QTest::qWait(250);
		QCOMPARE(mw.output()->toPlainText(), QString("KioqKioqKio="));
	}

	{
		qDebug() << "Decrypt";
		mw.data()->setPlainText("KioqKioqKio=");
		mw.key()->setPlainText("ABCDEFGH");
		mw.output()->setPlainText("CLEAN THIS");
		mw.data_direct_input()->setChecked(true);
		mw.key_direct_input()->setChecked(true);
		mw.output_direct_input()->setChecked(true);

		// data = base64
		mw.dataConverter()[MainWindow::CHKBOX_HEXA]->setChecked(false);
		mw.dataConverter()[MainWindow::CHKBOX_NONE]->setChecked(false);
		mw.dataConverter()[MainWindow::CHKBOX_BASE64]->setChecked(true);
		// key = none
		mw.keyConverter()[MainWindow::CHKBOX_BASE64]->setChecked(false);
		mw.keyConverter()[MainWindow::CHKBOX_HEXA]->setChecked(false);
		mw.keyConverter()[MainWindow::CHKBOX_NONE]->setChecked(true);
		// output = none
		mw.outputConverter()[MainWindow::CHKBOX_HEXA]->setChecked(false);
		mw.outputConverter()[MainWindow::CHKBOX_BASE64]->setChecked(false);
		mw.outputConverter()[MainWindow::CHKBOX_NONE]->setChecked(true);

		QVERIFY2(mw.cryptoSelector()->findText("XOR-CBC") != -1, "Unable to find XOR-CBC !");
		mw.cryptoSelector()->setCurrentIndex(mw.cryptoSelector()->findText("XOR-CBC"));
		mw._uncipher();
		for( int i=0; i<15 && mw.output()->toPlainText() == "CLEAN THIS"; ++i )
			QTest::qWait(250);
		QCOMPARE(mw.output()->toPlainText(), QString("ABCDEFGH"));
	}
}

//#include "CryptoTest.moc"
