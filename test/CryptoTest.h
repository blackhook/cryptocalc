#ifndef CRYPTOTEST_h
#define CRYPTOTEST_h
#include <QObject>
#include "Core/CryptoStream.h"


class CryptoTest: public QObject
{
	Q_OBJECT

	private:
		CryptoStream::CryptoStream_t m_stream;

	private:
		CryptoStream::CryptoStream_t* initStream( const QByteArray& input );

	private slots:
		void testCryptoSym();
		void testCryptoSym_data();

		void testGUI();

		void cleanupTestCase();
};

#endif// CRYPTOTEST_h
