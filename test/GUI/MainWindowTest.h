#ifndef MAINWINDOWTEST_H
#define MAINWINDOWTEST_H

#include "GUI/MainWindow.h"
#include <QPlainTextEdit>
#include <QRadioButton>
#include <QComboBox>

class MainWindowTest : public MainWindow
{
	public:
		QPlainTextEdit* data(){ return m_data; }
		QPlainTextEdit* key(){ return m_key; }
		QPlainTextEdit* output(){ return m_output; }
		QRadioButton* data_direct_input(){ return m_data_direct_input; }
		QRadioButton* key_direct_input(){ return m_key_direct_input; }
		QRadioButton* output_direct_input(){ return m_output_direct_input; }
		QRadioButton** dataConverter(){ return m_dataConverter; }
		QRadioButton** keyConverter(){ return m_keyConverter; }
		QRadioButton** key2Converter(){ return m_key2Converter; }
		QRadioButton** outputConverter(){ return m_outputConverter; }
		QComboBox* cryptoSelector(){ return m_cryptoSelector; }

		// SLOT
		void _cipher(){ cipher(); }
		void _uncipher(){ uncipher(); }
};

#endif// MAINWINDOWTEST_H
